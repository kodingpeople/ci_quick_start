/*Input box Plugin
--------------------------------------------------------------------------------------------------*/
(function($){
	$.fn.inputTextfield = function(){
	this.each(function(){
			var elm = $(this).find("input[type='text']");
			var defValue = $(elm).attr('value');
			$(elm).focus(function(){
				if($(this).val() == defValue)
				{
					$(this).val("");
					$(this).css('background','#fff');		
				}
			});
			$(elm).blur(function(){
				if($(this).val() == "")
				{
					$(this).val(defValue);	
				}
				$(this).css('background','#AAAAAA');
				
			});
			
			
		});	
		
	}
})(jQuery);

/*Select Box Plugin
--------------------------------------------------------------------------------------------------*/
(function($)
{
	$.fn.selectBox = function(options){
			var defaults = {  
				contWidth: 392,
				selectMenu: 392
				
		};  
			var opt = $.extend(defaults, options);  
	this.each(function(){
/*------Get Class of select box-------*/
	var prefex = $(this).attr('class');
	
	var selectClass = prefex.split(" ")[0];
	$(this).wrap('<div class="select_box"></div>');
	if($(this).hasClass('alignright'))
	{
		$('.'+selectClass).parent().css({'width':opt.contWidth,'float':'right'});	
	}
	
	$('.'+selectClass).css('width',opt.contWidth);
	$('.'+selectClass).parent().css('width', opt.contWidth);
	
	
	var defval = $('.'+selectClass+' option:selected').text();
	
	$('.'+selectClass).after('<span></span>');
	$('.'+selectClass).after('<em>'+defval+'</em>');
	
	$('.'+selectClass).change(function() {  //Change Value With Selected option
	
   // assign the value to a variable, so you can test to see if it is working
    var selectVal = $('.'+selectClass+' option:selected').text();   //Get Value of selected option
	
	$('.'+selectClass).next('em').remove(); // Remove Em
	
	$('.'+selectClass).after('<em>'+selectVal+'</em>'); // Add new Element(EM) with Value
	});
	
	})	
	}	
})(jQuery);