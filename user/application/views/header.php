<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>W-Address</title>
<link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/css/style.css" />
<link rel="stylesheet" href="<?=base_url()?>assets/css/chocolat.css" type="text/css" media="screen" charset="utf-8" />
<link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/css/media.css" />
<link rel="shortcut icon" href="<?=base_url()?>assets/images/favicon.png" type="image/x-icon" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />

<!-- Auto suggestion search -->
<link rel="stylesheet" href="<?=base_url()?>/assets/plugins/redmond/jquery-ui.css" />
<script src="<?=base_url()?>assets/plugins/js/jquery-1.9.1.js"></script>
<script src="<?=base_url()?>assets/plugins/js/jquery-ui.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("#Search_autosuggestion").autocomplete({
        source:'<?=base_url()?>users/Search_autosuggestion',
        minLength:0
    });
});
$(function() {
        $('#social_image a').Chocolat({overlayColor:'#222',leftImg:'assets/images/leftw.gif',rightImg:'assets/images/rightw.gif',closeImg:'assets/images/closew.gif'});
    });
</script>
<script src="<?=base_url()?>assets/js/jquery.chocolat.js"></script>
<!-- End auto suggestiion -->
<!--<script type="text/javascript" src="<?=base_url()?>assets/js/jquery-1.9.1.min.js"></script>-->
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.infieldlabel.min.js"></script>
<script src="<?=base_url()?>assets/js/easyResponsiveTabs.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="<?=base_url()?>assets/css/easy-responsive-tabs.css" />
<script type="text/javascript" charset="utf-8"> 
	$(function(){ $("label").inFieldLabels();});
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".loginbox").hide();
        $(".login").on('click',function(e){
            e.stopPropagation();
            if($('.loginbox').is(":visible")){
                $('.loginbox').slideUp(200);
                $(".login").removeClass("active")
            }else
            {
                $('.loginbox').slideDown();
                $(".login").addClass("active")
            }
        });
        
        $(".loginbox").on('click',function(e){
            e.stopPropagation();
        });
        
        /*Fire trigger if error founds*/
        <?php if(@$error || form_error('email') || form_error('password')) {
                    if($this->uri->segment(2) != 'signup'){?>
           // $(".loginbox").hide();
            $('.login').trigger('click');
        <?php }
        }?> 
        $("html").on('click',function(){
            $('.loginbox').slideUp(200);
            $(".login").removeClass("active")
        });
        
        
        $(".custombox").hide();
        $(".verify").on('click',function(e){
            e.stopPropagation();
            if($('.custombox').is(":visible")){
                $('.loginbox').fadeOut(200);
                //$(".verify").removeClass("active")
            }else
            {
                $('.custombox').fadeIn();
                //$(".verify").addClass("active")
            }
        });
        $(".custombox").on('click',function(e){
            e.stopPropagation();
        });
        $(".close a").on('click',function(){
            $('.custombox').fadeIn(200);
            //$(".verify").removeClass("active")
        });

       //$(".address_map").hide();
        $(".address_mapicon").on('click',function(e){
            e.stopPropagation();
            if($('.address_map').css('visibility') != 'hidden'){
                $('.address_map').css('visibility','hidden');
                $('.address_map').hide();
                $(".address_mapicon").removeClass("active")
            }else
            {
                $('.address_map').css('visibility','visible');
                $('.address_map').show();
                $(".address_mapicon").addClass("active")
            }
        });
        
        $(".address_map").on('click',function(e){
            e.stopPropagation();
        });
        $("html").on('click',function(){
            $('.address_map').slideUp(200);
            $(".address_mapicon").removeClass("active")
        });

        $(".wowdealpromotions").hide();
          $(".promotions").on('click',function(e){
           e.stopPropagation();
           if($('.wowdealpromotions').is(":visible")){
            $('.loginbox').fadeOut(200);
            //$(".verify").removeClass("active")
           }else
           {
            $('.wowdealpromotions').fadeIn();
            e.stopPropagation();
            //$(".verify").addClass("active")
           }
          });
          $(".wowdealpromotions").on('click',function(e){
           e.stopPropagation();
          });
          $(".close a").on('click',function(){

           $('.wowdealpromotions').fadeOut(200);
           //$(".verify").removeClass("active")
             e.stopPropagation();
          });
    });
</script>
<!--[if lte IE 8]>
    <script type="text/javascript" src="<?=base_url()?>assets/js/html5.js"></script>
<![endif]-->
<!--[if IE 8]>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/ie8.css">
<![endif]-->
<?php if($this->uri->segment(3) == '17'){?>
<!-- Chang URLs to wherever Video.js files will be hosted -->
  <link href="<?=base_url()?>assets/plugins/video-js/video-js.css" rel="stylesheet" type="text/css">
  <!-- video.js must be in the <head> for older IEs to work. -->
  <script src="<?=base_url()?>assets/plugins/video-js/video.js"></script>
  <!-- Unless using the CDN hosted version, update the URL to the Flash SWF -->
  <script>
    videojs.options.flash.swf = "video-js.swf";
  </script>
  
<?php }?>
</head>
<body>
  
<div class="waddress_bg">
<header>
	<div class="top_header">
   	  	<div class="centerwrap">
        <div class="bluebg">
                <div class="header_right">
                    <div class="toplinks_left">
                        <a href="<?=base_url('home')?>" class="home active" title="<?=$this->lang->line('toltip_home')?>"><span></span></a>
                        <?php if($this->session->userdata('waddress_logged_in') != TRUE){?>
                        <div class="loginarea">
                            
                            <a href="javascript:void(0)" class="login" title="<?=$this->lang->line('toltip_login')?>"><span></span></a>
                            <?php echo form_open('users/login', array('name'=>"frm_login", 'class'=>"form-horizontal")); ?>
                            <div class="loginbox">
                                <div class="reletive">
                                    <?php if(@$error){?>
                                        <div class="error">
                                            <ul align="center">
                                                <?php echo @$error;?>
                                            </ul>
                                        </div>
                                    <?php } ?>
                                    <div class="toparrow"><img src="<?=base_url('assets/images/toparrow.png')?>" alt=""></div>
                                    <div class="texticonbox">
                                        <span class="usericon"><img src="<?=base_url('assets/images/user_icon.png')?>" alt=""></span>
                                        <label for="email_lg">Username</label>
                                        <input name="email_lg" id="email_lg" autocomplete="off" type="text" value="<?=get_cookie($this->config->item('cookie_prefix').'userEmail')?>"/>
                                        <?php echo form_error('email_lg'); ?>
                                    </div>
                                    <div class="texticonbox">
                                        <span class="passwordicon"><img src="<?=base_url('assets/images/password_icon.png')?>" alt=""></span>
                                        <label for="password">Password</label>
                                        <input name="password" id="password" autocomplete="off" type="password" value="<?=get_cookie($this->config->item('cookie_prefix').'userPassword')?>"/>
                                        <?php echo form_error('password'); ?>
                                    </div>
                                    <label class="remember"><input name="rememberMe" type="checkbox" value="remember_me" checked="checked"> Remember Me</label>
                                    <p class="forgot"><a href="<?=base_url('users/forgotpassword')?>" title="Forgot Password">Forgot Password ?</a></p>
                                    <div class="clear"></div>
                                    <input type="submit" value="Login" class="loginbtn">
                                </div>
                            </div>
                            <?php echo form_close(); ?> 
                        </div>
                            <?php } else {?> 

                           <a href="<?=base_url('users/logout');?>" class="logoutclass"><span></span></a>
                             <?php }?>

                        <a href="<?=base_url('pages/cms/17/')?>" class="vidoe" title="<?=$this->lang->line('toltip_video')?>"><span></span></a>
                        <div class="clear"></div>
                    </div>
                    <div class="top_logo"><a href="<?=base_url('pages/cms/11')?>" target="_blank" title="<?=$this->lang->line('toltip_terms')?>"><img src="<?=base_url()?>assets/images/top_logo_small.png" alt="" /></a></div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="logo"><a href="<?=site_url('home');?>" title="<?=$this->lang->line('toltip_waddress')?>"><img src="<?=base_url('assets/images/logo.png')?>" alt="" /></a></div>
            <nav class="topnav">
                <ul>
                    <li>Design<span><img src="<?=base_url('assets/images/topnavdot.png')?>" alt="" /></span></li>
                    <li>Display<span><img src="<?=base_url('assets/images/topnavdot.png')?>" alt="" /></span></li>
                    <li>Discovery</li>
                </ul>
            </nav>

            <div class="clear"></div>
      	</div>
  	</div>
    <div class="top_shadow"></div>
    <div class="findfriends centerwrap">
    	<div class="wsearchlogo"><img src="<?=base_url()?>assets/images/logo_search.jpg" alt="" /></div>
        <div class="wsearchbg">
        <?php 
         $attributes = array('class' => 'form-horizontal', 'id' => 'frm_search');
         echo form_open('search',$attributes); ?>
         <div class="wsearchbox"><span></span><input name="keyword" type="text" class="textbox" id="Search_autosuggestion" placeholder="" name="Search_autosuggestion" value="<? //=@$_SESSION['keyword'] ?>" /></div>
       
            <input type="submit" value="Connect" class="connect" />
             <?php echo form_close()?> 
           
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <!-- Message box for success/warning/error-->
            <?php   
            if($this->session->flashdata('search_error')){?>
                        <span class="search_error">
                              <?php  echo $this->session->flashdata('search_error'); ?>
                        </span>
            <?php } ?>
</div>
</header>
  