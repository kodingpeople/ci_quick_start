<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.tooltip.js"></script>
<script type="text/javascript">
      $j = jQuery.noConflict();
      $j(document).ready(function(){
        $j("div.item").tooltip();
      });
</script>
<script type="text/javascript">
 
 $(document).ready(function() {
 
    $('#accountpassword').keyup(function(){
        $('#result').html(checkStrength($('#accountpassword').val()))
    })  
 
    function checkStrength(password){
 
    //initial strength
    var strength = 0
 
    //if the password length is less than 6, return message.
    if (password.length < 6) {
        $('#result').removeClass()
        $('#result').addClass('short')
        return 'Too short'
    }
 
    //length is ok, lets continue.
 
    //if length is 8 characters or more, increase strength value
    if (password.length > 12) strength += 1
 
    //if password contains both lower and uppercase characters, increase strength value
    if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))  strength += 1
 
    //if it has numbers and characters, increase strength value
    if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))  strength += 1 
 
    //if it has one special character, increase strength value
    if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))  strength += 1
 
    //if it has two special characters, increase strength value
    if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,",%,&,@,#,$,^,*,?,_,~])/)) strength += 1
 
    //now we have calculated strength value, we can return messages
 
    //if value is less than 2
    if (strength < 2 ) {
        $('#result').removeClass()
        $('#result').addClass('weak')
        return 'Weak'
    } else if (strength < 4 ) {
        $('#result').removeClass()
        $('#result').addClass('good')
        return 'Good'
    } else {
        $('#result').removeClass()
        $('#result').addClass('strong')
        return 'Strong'
    }
}
}); 

</script>
<!-- <section class="custombox">
	<div class="blackarea"></div>
	<div class="lightbox">
  		<div class="thankyou">
        	<h3>Thank you</h3>
            <h4>Please check your email <span>(xxxx@xxx.xom)</span> to confirm your identity.</h4>
          	<p>Once you confirm your registration, you will be able to login to W-address to claim your unique identity on W-Address. If you did not received your confirmation email, please email us at <a href="mailto:w-address@waddress.com">W-Address@waddress.com</a></p>
            <div class="close"><a href="#"><img src="<?=base_url()?>/assets/images/close.png" alt=""></a></div>
        </div>
    </div>
</section> -->
<?php echo form_open('front/signup', array('name'=>"frm_register", 'class'=>"form-horizontal")); ?>
<section>
    <div class="whitebg centerwrap paddingb20">
   	  	<div class="createaccount">
        	<div class="accounttitle"><span class="titleft"><img src="<?=base_url()?>/assets/images/titleleftbg.jpg" alt=""></span>Create Account<span class="titleright"><img src="<?=base_url()?>/assets/images/titlerightbg.jpg" alt=""></span></div>
      		<div class="createform">
            	 <div class="textboxarea">
                  
                  <label for="last_name">*User name for loggin</label>
                    <input name="last_name" id="last_name" type="text"  value="<?php echo set_value('last_name'); ?>" />
                    <?php echo form_error('last_name'); ?>
                  </div>

                  <div class="textboxarea">
                    	<label for="email_reg">*E-mail</label>
                       	<input name="email" id="email_reg" type="text"  value="<?php echo set_value('email'); ?>" />
                        <?php echo form_error('email'); ?>
                    </div>
  				        
                  <div class="textboxarea">
                  	<label for="repeat_email">*Repeat E-mail</label>
                     <input name="repeat_email" id="repeat_email" type="text"  value="<?php echo set_value('repeat_email'); ?>" />
                      <?php echo form_error('repeat_email'); ?>
                  </div>
          				
                  <div class="textboxarea">
                        	<label for="accountpassword">*Password</label>
                           	<input name="password" id="accountpassword" type="password"  value="<?php echo set_value('password'); ?>" />
                            <?php echo form_error('password'); ?>
                  </div>

          				<div class="textboxarea">
                        	<label for="repeat_password">*Repeat Password</label>
                           	<input name="repeat_password" id="repeat_password" type="password"  value="<?php echo set_value('repeat_password'); ?>" />
                            <?php echo form_error('repeat_password'); ?>
                            <?php echo form_error('matches'); ?>
                  </div>
                  <!-- <p class="tooshort"><span id="result"></span><a href="#"><img src="<?=base_url()?>/assets/images/que.png" alt=""></a></p> -->
                   
                   <div class="tooltiparea">
                     <div class="item" id="item_4"><a><img src="<?=base_url()?>/assets/images/que.png" alt="" /></a>
                     <div class="tooltip_description" style="display:none">
                            <p>Password can contain lower-and uppercase english letters, numbers and special characters. Any other symbols are not allowed.</p>
                            <p><strong>We highly recommend using a strong password</strong> to protect your account and your accounts which we connect to. Please follow the recommendations below to enter a strong password:</p>
                            <ul>
                                <li>Contains both small and uppercase letters</li>
                                <li>Contains at least one numerical character</li>
                                <li>Contains special characters</li>
                                <li>Has more than 12 characters</li>
                            </ul>
                      </div>
                      </div>
                      <p class="tooshort"><span id="result"></span></p>
                    </div> 
                
                  <div class="textboxarea" style="display:none">
                  	<label for="first_name">First Name</label>
                     	<input name="first_name" id="first_name" type="text"  value="<?php echo set_value('first_name'); ?>" />
                      <?php echo form_error('first_name'); ?>
                  </div>
    				     
          				<!-- <div class="textboxarea">
                        	<label for="area_code">*Area Code</label>
                           	<input name="area_code" id="area_code" type="text"  value="<?php echo set_value('area_code'); ?>" />
                          <?php echo form_error('area_code'); ?>
                                </div> -->
          				<div class="textboxarea" style="display:none">
                        	<label for="phone">Phone No.</label>
                           	<input name="phone" id="phone" type="text"  value="<?php echo set_value('phone'); ?>" />
                            <span class="donotdisplay"> Do not complete if you do not want your number published</span>
                          <?php echo form_error('phone'); ?>

                 </div>

                 <div class="textboxarea citystate" style="display:none">
                          <label for="city">City</label>
                            <input name="city" class="citystate marginright" id="city" type="text"  value="<?php echo set_value('city'); ?>" />
                            <span class="donotdisplay"> Do not complete if you do not want your City published</span>
                          <?php echo form_error('city'); ?>

                 </div>

               <div class="textboxarea citystate marginright" style="display:none">
                        <label for="state">State</label>
                          <input name="state" id="state" type="text" class="citystate" value="<?php echo set_value('state'); ?>" />
                          <span class="donotdisplay"> Do not complete if you do not want your State published</span>
                        <?php echo form_error('state'); ?>

               </div>

              <div class="textboxarea" style="display:none">
              	<label for="zipcode">Zip Code</label>
                 	<input name="zipcode" id="zipcode" type="text"  value="<?php echo set_value('zipcode'); ?>" />
                 <!--  <span class="donotdisplay"> Do not complete if you do not want your number published</span> -->
                 <span class="donotdisplay"> Do not complete if you do not want your Zip Code published</span>
                <?php echo form_error('zipcode'); ?>
              </div>
   <?php if($userplan == 'commercial'){?>
            
              <div class="textboxarea" style="display:none">
                  <p>Payment Type :- </p>
                  <span> Auto <input type="radio" name="paymentType" id="paymentType" value="a" <?php echo set_radio('paymentType', 'a'); ?> /></span>
                  <span> Manually <input type="radio" name="paymentType" id="paymentType" value="m" <?php echo set_radio('paymentType', 'm',TRUE); ?> /></span>
                  <?php echo form_error('paymentType'); ?>
              </div>
  <?php }?>
          </div>
        	<p class="byclicking">By clicking this button you agree to W-Address <a href="<?=base_url('pages/cms/11')?>" target="_" title="<?=$this->lang->line('toltip_terms')?>">Terms of Use</a> and <a href="<?=base_url('pages/cms/13')?>" target="_" title="<?=$this->lang->line('toltip_privacystate')?>">Privacy Policy</a>.</p>
        	<input type="submit" value="verify identity" class="verify">
   	  	</div>
        <div class="boxshadow"><img src="<?=base_url()?>/assets/images/box_shadow.png" alt=""></div>
    </div>
</section>
<?php echo form_hidden('userplan', $this->session->userdata('plantypeReg'));?>
<?php echo form_close(); ?> 
