<section>
    <div class="whitebg centerwrap paddingb20">
   	  	<div class="staticarea">
        	<h2><?=$page->title;?></h2>
        	<?php if($this->uri->segment(3) == '17'){?>
			    <div align="center">
			    <video id="example_video_1" class="video-js vjs-default-skin vjs-big-play-centered"
  controls preload="auto" width="auto" height="529"
  poster=""
  data-setup='{"example_option":true}'>
			    <source src="<?=base_url()?>assets/video/video.mp4" type='video/mp4' />
			    <source src="<?=base_url()?>assets/video/video.webm" type='video/webm' />
			    <source src="<?=base_url()?>assets/video/video.ogv" type='video/ogg' />
			    <!-- <track kind="captions" src="demo.captions.vtt" srclang="en" label="English"></track><!-- Tracks need an ending tag thanks to IE9 -->
			    <!-- <track kind="subtitles" src="demo.captions.vtt" srclang="en" label="English"></track> --><!-- Tracks need an ending tag thanks to IE9 --> -->
			  </video>
			  </div>
        		<br/><br/>
        	<?php }?>

          <?=htmlspecialchars_decode($page->text);?>
       </div>

    </div>
		<!-- Unless using the CDN hosted version, update the URL to the Flash SWF -->
</section> 