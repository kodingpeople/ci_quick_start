<?php 
$verifyUrl = base_url('users/verifyidentity/?key='.base64_encode($row->last_name)); ?>

<!-- Hi,
<?php echo $row->first_name;?>
You have been notified that your successfully Register in Our site

Email Id : <?php echo $row->email;?> 
but you cant login before verify your identity for verify identity click <a href="<?php echo $verifyUrl; ?>">here</a> 

Thank you for using our services!

With Regards
Waddress Team
 -->


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Mail Confirmation - W-Address Team</title>
<style>
a{color:#1155CC;}
</style>
</head>
<body>
<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="border:1px solid #CCCCCC">
  <tr>
    <td>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td style="padding-left:15px; padding-top:17px; border-top:4px solid #000"><img src="<?=base_url('assets/images/logo.png');?>" width="195" height="43" /></td>
      </tr>
      <tr><td height="30"></td></tr>
      <tr>
      <!--  color:#b3b3b3 -->
        <td style="font-family:calibri, Helvetica, sans-serif; font-size:16px;; line-height:18px; padding:0 25px;">
     <!--  Hi <strong>Zalak Patel</strong>,<br /><br /> -->
      <!-- Welcome to W-Address!!<br /><br /> -->
      <!-- This is to notified that you are successfully registered in our site.  -->
      Welcome and thank you for subscribing to W-Address.<br/>
      You are successfully registered as <strong><?php echo $row->email;?></strong>
      <br/><br/>
      Please confirm this e-mail to activate and set up your account<br/>
      <a href="<?php echo $verifyUrl; ?>">click here for Verify</a>
      <br/><br/>
      <!-- <br/>Thank you for using our services.<br />
      For any queries please refer to W-Address help.<br /><br /><br />
 -->
      <div style=" border: 4px double #999;border-bottom: 4px double #999;padding: 11px 6px;margin-bottom: 10px;margin-top: 10px;border-radius: 10px;"><span style="color:#F00; text-decoration:underline">W</span><span style="color:#F00;">-Address Top Tips:</span><br />
      1.A W-Address must be at least 2 alpha-numeric characters in length <br/>
      2.Underscores and dashes are the only two symbols allowed in W-Address<br/>
      3.To improve result and to ensure easier W-Address creation we suggest using all or part of an address or phone number when creating a Social Commerce label to use on items or property For Sale,Rent or Lease.<br/>
      4.Last, but certainly not least, W-Addresses CAN NOT BE BOUGHT and SOLD.Please NO CYBER SQUATTING as we will pursue aggressive legal action <br/>
</div>
<br />
Please refer to FAQ's,Terms and Privacy for additional
information regarding  this service
      
  <br />
  <br />
  Best of luck!<br /><br />
  <p style="text-align:center; margin:0px; font-size:11px; color:#999; padding-bottom:3px;">Copyrights 2014 W-Address.com All Rights Reserved.</p>
      </td></tr>
  </table>
  </td>
  </tr>
</table>
</body>
</html>