 <script type="text/javascript">
 $(document).ready(function(){
  $(".loginbox").hide();
  });
 </script>

	
<?php echo form_open('users/forgotpassword', array('name'=>"forgotpassword", 'class'=>"form-horizontal")); ?>
<section>
    <div class="whitebg centerwrap paddingb20">
   	  		<!-- messages section (error, warning, success) -->

<?php if($this->session->flashdata('success')) : ?>
	<div class="alert alert-success">
		<?php echo $this->session->flashdata('success')?>
  </div>
 <?php endif; ?>
<!-- /messages section -->	
   	  	<div class="createaccount">
        	<div class="accounttitle"><span class="titleft"><img src="<?=base_url()?>/assets/images/titleleftbg.jpg" alt=""></span>Password Recovery Process<span class="titleright"><img src="<?=base_url()?>/assets/images/titlerightbg.jpg" alt=""></span></div>
      		 <div class="createform">
            	<div class="textboxarea">
                	<label for="email_fp">*Enter Your Email  Address </label>
                   	<input name="email_fp" id="email_fp" type="text"  value="<?php echo set_value('email'); ?>" />
                    <?php echo form_error('email_fp'); ?>
                    <?php /*if($this->session->flashdata('error')) {?>
                        <span class="error">
                        <?php 
                        echo $this->session->flashdata('error');
                        ?>
                    </span>
                    <?php }*/?>
                </div>
                <div class="textboxarea">
                  <label for="subscribername_fp">*Subscribers name </label>
                    <input name="subscribername_fp" id="subscribername_fp" type="text"  value="<?php echo set_value('subscribername_fp'); ?>" />
                    <?php echo form_error('subscribername_fp'); ?>
                    <?php if($this->session->flashdata('error')) {?>
                        <span class="error">
                        <?php 
                        echo $this->session->flashdata('error');
                        ?>
                    </span>
                    <?php }?>
                </div>
                
          	</div>
        	<input type="submit" value="<?php echo $this->lang->line('send');?>" class="verify">
          <div align="center"><p class="byclicking"><a href="<?=base_url();?>" title="<?=$this->lang->line('toltip_dontacc')?>">Don't have a W-Address account ?</a></p></div>
   	  	</div>
        <div class="boxshadow"><img src="<?=base_url()?>/assets/images/box_shadow.png" alt=""></div>
    </div>
</section>

<?php echo form_close(); ?> 