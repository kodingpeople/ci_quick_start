<?php
	/**
	 * @name: form.php
	 * 
	 * @desc: Load new user registration form
	 * 
	 * @author: Bhavesh Khanpara
	 */
?>
<!-- messages section (error, warning, success) -->
<?php 

if (validation_errors()): ?>
	<div class="alert alert-error">
		<ul><?php echo validation_errors(); ?></ul>
	</div>
<?php elseif(@$error) : ?>
	<div class="alert alert-error"><ul><?php echo $error;?></ul></div>
<?php else : ?><div class="alert alert-info">
	<?php echo $this->lang->line('page_register');?>
	</div>
<?php endif; ?>
<!-- /messages section -->	

<?php echo form_open('front/signup', array('name'=>"frm_register", 'class'=>"form-horizontal")); ?>


<fieldset>
<div class="input-prepend" title="email" data-rel="tooltip">
<input name="email" id="email" type="text"  value="<?php echo set_value('email'); ?>" />
</div>

<div class="input-prepend" title="repeat email" data-rel="tooltip">
<input name="repeat_email" id="repeat_email" type="text"  value="<?php echo set_value('repeat_email'); ?>" />
</div>

<div class="input-prepend" title="password" data-rel="tooltip">
<input name="password" id="password" type="text"  value="<?php echo set_value('password'); ?>" />
</div>

<div class="input-prepend" title="repeat password" data-rel="tooltip">
<input name="repeat_password" id="repeat_password" type="text"  value="<?php echo set_value('repeat_password'); ?>" />
</div>


<div class="input-prepend" title="first name" data-rel="tooltip">
<input name="first_name" id="first_name" type="text"  value="<?php echo set_value('first_name'); ?>" />
</div>

<div class="input-prepend" title="Last name" data-rel="tooltip">
<input name="last_name" id="last_name" type="text"  value="<?php echo set_value('last_name'); ?>" />
</div>

<div class="input-prepend" title="area_code" data-rel="tooltip">
<input name="area_code" id="area_code" type="text"  value="<?php echo set_value('area_code'); ?>" />
</div>

<div class="input-prepend" title="Phone No" data-rel="tooltip">
<input name="phone" id="phone" type="text"  value="<?php echo set_value('phone'); ?>" />
</div>

<div class="input-prepend" title="City" data-rel="tooltip">
<input name="city" id="city" type="text"  value="<?php echo set_value('city'); ?>" />
</div>

<div class="input-prepend" title="state" data-rel="tooltip">
<input name="state" id="state" type="text"  value="<?php echo set_value('state'); ?>" />
</div>

<div class="input-prepend" title="Zipcode" data-rel="tooltip">
<input name="zipcode" id="zipcode" type="text"  value="<?php echo set_value('zipcode'); ?>" />
</div>


<div class="clearfix"></div>
<p class="center span5">
<button type="submit" class="btn btn-primary"><?php echo $this->lang->line('save');?></button>
</p>
</fieldset>


<?php echo form_close(); ?>	