 <script type="text/javascript">
 $(document).ready(function(){
 $(".loginbox").hide();
 });
 </script>
<?php
/**
	 * @name: users/form.php
	 * 
	 * @desc: view/edit orders main listing view users for admin
	 * 
	 * @author: Bhavesh Khanpara
	 */
?>
<?php echo form_open('users/changepassword', array('name'=>"changepassword", 'class'=>"form-horizontal")); ?>
<section>
    <div class="whitebg centerwrap paddingb20">
   	  		<!-- messages section (error, warning, success) -->
<?php if(validation_errors()){?>
					<div class="alert alert-error">
						<ul>
							<?php //echo validation_errors(); ?>
						</ul>
					</div>
					<?php } ?>
<!-- /messages section -->	
   	  	<div class="createaccount">
        	<div class="accounttitle"><span class="titleft"><img src="<?=base_url()?>/assets/images/titleleftbg.jpg" alt=""></span>Password change Process<span class="titleright"><img src="<?=base_url()?>/assets/images/titlerightbg.jpg" alt=""></span></div>
      		<div class="createform">
            	<div class="textboxarea">
                	<label for="email">*E-mail</label>
                   	<input name="email" id="email" type="text"  value="<?php echo (isset($query->email))?$query->email:'';?><?php echo set_value('email'); ?>" />
                    <?php echo form_error('email'); ?>
                </div>

                <div class="textboxarea">
                	<label for="old_password">*old_password</label>
                   	<input name="old_password" id="old_password" type="text"  value="<?php echo set_value('old_password'); ?>" />
                    <?php echo form_error('old_password'); ?>
                </div>

                 <div class="textboxarea">
                	<label for="new_password">*new_password</label>
                   	<input name="new_password" id="new_password" type="text"  value="<?php echo set_value('new_password'); ?>" />
                    <?php echo form_error('new_password'); ?>
                </div>
  				
          	</div>
        	
        	<div class="form-actions">
						<input name="submit" class="verify" type="submit" value="<?php echo $this->lang->line('save');?>">
						<!-- <a href="<?php echo base_url(); ?>users/" class="btn">Cancel</a> -->
					</div>
   	  	</div>
        <div class="boxshadow"><img src="<?=base_url()?>/assets/images/box_shadow.png" alt=""></div>
    </div>
</section>

<?php echo form_close(); ?> 
		

		