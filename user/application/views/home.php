   <!-- List of message that will be disply on home par depending on cases -->
    <?php   
    //print_r($this->session->all_userdata());
    if($this->session->flashdata('info')){?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('info')?>
        </div>
    <?php } ?>

    <?php if($this->input->get('action') == 'logout'){?>
        <div class="logout_alert">
            <?php echo $this->lang->line('logout_message');?>
            <?php //echo 'You are successfully logged out from Our system..'?>
        </div>
    <?php }?>

     <?php if($this->input->get('action') == 'deleted'){?>
        <div class="logout_alert">
            <?php echo $this->lang->line('user_notexits');?>
        </div>
    <?php }?>
     <?php if($this->input->get('action') == 'deleteAccount'){?>
        <div class="alert alert-success">
            <?php echo $this->lang->line('deleteAccount_message');?>
            <?php //echo 'Your Account was successfully deleted from our system..'?>
        </div>
    <?php }?>
    <?php if($this->input->get('result') == 'empty'){?>
        <div class="alert alert-success">
            <?php echo $this->lang->line('empty_message');?>
            <?php //echo 'Your Account was successfully deleted from our system..'?>
        </div>
    <?php }?>

    <?php if($this->input->get('msg') == 'secondAttempt'){?>
        <div class="alert alert-success">
            <?php echo $this->lang->line('message_secondAttempt');?>
            <?php //echo 'Your Account was successfully deleted from our system..'?>
        </div>
    <?php }?>

    <?php if($this->input->get('msg') == 'recorednotfound'){?>
        <div class="alert alert-success">
            <?php echo $this->lang->line('user_notexits');?>
            <?php //echo 'Your Account was successfully deleted from our system..'?>
        </div>
    <?php }?>
<!-- Message box for success/warning/error-->
 <script src="http://api.html5media.info/1.1.6/html5media.min.js"></script>
<section>
    <div class="whitebg centerwrap paddingb45 minheight">
    	<div class="social_icons">
        	<div class="social_icon"><img src="<?=base_url()?>assets/images/social_icons.png" alt="" /></div>
            <div class="welcome_text">
                <div class="toptext">
                 <h3>Wa-dress  <span>/wa/dress/</span></h3>
                    <h5>
                    <!-- <audio id="player" src="<?=base_url()?>/assets/test.mp3"></audio>
                    <div>
                        <button onclick="document.getElementById('player').play()">Play</button>
                        <button onclick="document.getElementById('player').pause()">Pause</button>
                        <button onclick="document.getElementById('player').volume+=0.1">Volume Up</button>
                        <button onclick="document.getElementById('player').volume-=0.1">Volume Down</button>
                    </div>  -->
                   <!--  <audio src="<?=base_url()?>assets/test.mp3" controls preload></audio> -->

                    </h5>
                    <div class="clear"></div>
                </div>
            	<p>A unique, alphanumeric ID, label or name symbolized by an underlined <span class="wordlogo">W</span> which seamlessly connects to a self created, mobile device optimized web page when entered into the data field at <span>W-Address.com</span></p>
            </div>
            <div class="clear"></div>
        </div>
            <div class="pscblocks">
       
                <div class="blocks">
                	<div class="title">Personal</div>
                	<div class="whitebox">
                    	<div class="tag personal_tag"><?=$this->config->item('personal_plan');?></div>
                    	<div class="blockimg"><img src="<?=base_url()?>/assets/images/personal_icon.png" alt="" /></div>
                        <div class="blockdata">
                        	<p>
                            <!-- Create a unique, personalized ID and upload your contact info as well as multiple Social Media connections on a self created, semi-customizable web page. -->
                            Create a personalized ID,input contact info,upload a photo,relevant content
and select various Social media connections to display.The subscription is FREE and can be 
updated or edited anytime.
                            </p>
                            <a href="<?=site_url('front/signup/personal')?>" class="create" title="<?=$this->lang->line('toltip_create')?>">Create</a>
                        </div>
                    	<div class="clear"></div>
                  	</div>
                </div>  
            <div class="blocks">
            	<div class="title">Social Commerce</div>
            	<div class="whitebox">
                	<div class="tag social_tag"><!-- <p>$<?=$this->config->item('social_commerce_plan');?> <span>6 months</span></p>-->Free</div>
                	<div class="blockimg"><img src="<?=base_url()?>/assets/images/social_icon.png" alt="" /></div>
                    <div class="blockdata">
                    	<p><!-- Produce an appropriate W-Address label and post it on signage in clear public view to improve results with tangible items or property For Sale, Rent or Lease and/or to provide information and directions to Estate and Garage Sales. The subscription is FREE for 60 days and renewable at $<?=$this->config->item('social_commerce_plan');?>/month up to an additional 4 months. -->
                            Publicize suitable lables on signs to draw attention to items or 
property For Sale,Rent or Lease and provide directions to 
gatherings,Garage and Estate Sales. Subscriptions are FREE for 60 days and renewable at $<?=$this->config->item('social_commerce_plan');?>/month up to 4 
additional months.
                        </p>
                        <a href="<?=site_url('front/signup/social_commerce')?>" class="create" title="<?=$this->lang->line('toltip_create')?>">Create</a>
                    </div>
                	<div class="clear"></div>
              	</div>
            </div>  
            <div class="blocks last">
            	<div class="title">Commercial</div>
            	<div class="whitebox">
                	<div class="tag commercial_tag"><p>$<?=$this->config->item('commercial_plan');?><span>per year</span></p></div>
                	<div class="blockimg"><img src="<?=base_url()?>/assets/images/commercial_icon.png" alt="" /></div>
                    <div class="blockdata">
                    	<p><!-- Business entities can purchase one or multiple, brand specific W-Address names and modify each one with timely, relevant content to effectively geo-target the burgeoning, mobile device centric culture. Aggregate all Social Media connections in one place and take advantage of the inclusive Social Buying tool called "WOW Deals". Renewable subscriptions are $<?=$this->config->item('commercial_plan');?>/year. -->
                            Commercial subscriptions are $<?=$this->config->item('commercial_plan');?>/year and will cost
effectively and informatively introduce consumers to your product,service or profession as well as stimulate Social Media
activity.Innovatively purchase several brand related names to effectively geo-target numerous markets simultaneously.And 
take advantage of "WOW Deals" the inclusive marketing tool designed to boost business with Social buying deals. 
                        </p>
                        <a href="<?=site_url('front/signup/commercial')?>" class="create" title="<?=$this->lang->line('toltip_create')?>">Create</a>
                    </div>
                	<div class="clear"></div>
              	</div>
            </div>
        </div>
    </div>
</section>