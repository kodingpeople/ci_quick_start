<?php
/*Tooltip */
$lang['toltip_home'] = "Home";
$lang['toltip_login'] = "Login";
$lang['toltip_video'] = "Video";
$lang['toltip_terms'] = "Terms";
$lang['toltip_waddress'] = "W-Address | The Mobile Web Address";
$lang['toltip_contact'] = "Contact";
$lang['toltip_privacy'] = "Privacy Policy";
$lang['toltip_privacystate'] = "Privacy Statement";
$lang['toltip_create'] = "Create";
$lang['toltip_logout'] = "Logout";
$lang['toltip_twitter'] = "Twitter";
$lang['toltip_googleplus'] = "Googleplus";
$lang['toltip_flickr'] = "Flickr";
$lang['toltip_facebook'] = "Facebook";
$lang['toltip_linkedin'] = "Linkedin";
$lang['toltip_tumblr'] = "Tumblr";
$lang['toltip_youtube'] = "Youtube";
$lang['toltip_instagram'] = "Instagram";
$lang['toltip_foursquare'] = "Foursquare";
$lang['toltip_googlemap'] = "Googlemap";
$lang['toltip_dontacc'] = "Don't have a W-Address account ?";
$lang['toltip_cancel'] = "Cancel";
$lang['toltip_addmore_waddress'] = "Click here to Add more W-Address";
$lang['toltip_yelp'] = "Yelp";
$lang['toltip_pinterest'] = "Pinterest";
$lang['toltip_Backtomyaccount'] = "Back to My account";
$lang['toltip_paymentinfo'] = "Payment Info";
$lang['toltip_Business'] = "Business";
$lang['toltip_SocialConnections'] = "Social Connections";
$lang['toltip_deal'] = "Deal";

$lang['toltip_LableName'] = "ID,Label or Name to display on page";
$lang['toltip_typeinfo'] = "Type info to display on page";
$lang['toltip_webaddresslink'] = "Web Address/link. Use http:// or https:// format only";
$lang['toltip_phoneformate'] = "International phone number format";
/*Tooltip */

// Display message on Home page
$lang['user_notexits'] 			 = "Your account has been deleted.Sorry to see you go!";
$lang['deleteAccount_message']	 = "Your account has been deleted.Sorry to see you go!";
$lang['empty_message'] 			 = "Your account has been deleted.Sorry to see you go!";
$lang['logout_message'] 		 = "You've successfully logged out";
//$lang['message_secondAttempt'] 		 = "Your Mail verification process was already completed..please login with your credential!";
$lang['message_secondAttempt'] 		 = "Your e-mail verfication is complete -Please login with your credentials";
$lang['work_on_mobile'] 		 = "Phone option only works with a mobile device";
$lang['text_accout_activate_chkbox'] 		 = "click/tap to publish your W-Address page";
$lang['msg_creation_criteria'] 		 = "<b>Creation criteria:Minimum 2 alpha-numeric characters - Dash and underscore symbols only<b/>";

/*Social Description*/
$lang['socialTab_Form_desc_googlemap'] 	= 'Please cut and paste or type your Google Map address and press SUBMIT.We DO NOT store or share 3rd party data<br/>Thank you!';
$lang['socialTab_Form_desc_youtube'] 	= 'In order to activate your Youtube video please type or cut and paste your uploaded video url in the box below and press AUTHORIZE YOUTUBE.We DO NOT store or share 3rd party data</br>Thank you!.';
$lang['socialTab_Form_desc_yelp'] 		= 'Please cut and paste or type your Yelp link in the box and press SUBMIT.We DO NOT store or share 3rd party data<br/>Thank you!';
$lang['socialTab_Form_desc_pinterest'] 	= 'In order to activate your Pinterest account please type or cut and paste your personal Pinterest link in the box below and press Submit.We DO NOT store or share 3rd party data</br>Thank you!.';
$lang['socialTab_Form_desc_googleplus'] = 'Click or tap "Authorize Google plus" to authenticate and allow activation on your W-Address page. We DO NOT store or share third party data or information<br/>Thank you!';
$lang['socialTab_Form_desc_facebook'] 	= 'Click or tap "Authorize Facebook" to authenticate and allow activation on your W-Address page. We DO NOT store or share third party data or information<br/>Thank you!';
$lang['socialTab_Form_desc_linkedin'] 	= 'Click or tap "Authorize Linkedin" to authenticate and allow activation on your W-Address page. We DO NOT store or share third party data or information<br/>Thank you!';
$lang['socialTab_Form_desc_twitter'] 	= 'Click or tap "Authorize Twitter" to authenticate and allow activation on your W-Address page. We DO NOT store or share third party data or information<br/>Thank you!';
$lang['socialTab_Form_desc_flickr'] 	= 'Click or tap "Authorize Flickr" to authenticate and allow activation on your W-Address page. We DO NOT store or share third party data or information<br/>Thank you!';
$lang['socialTab_Form_desc_tumblr'] 	= 'Click or tap "Authorize Tumblr" to authenticate and allow activation on your W-Address page. We DO NOT store or share third party data or information<br/>Thank you!';
//$lang['socialTab_Form_desc_youtube'] 	= 'Click or tap "Authorize Youtube" to authenticate and allow activation on your W-Address page. We DO NOT store or share third party data or information<br/>Thank you!';
$lang['socialTab_Form_desc_instagram'] 	= 'Click or tap "Authorize Instagram" to authenticate and allow activation on your W-Address page. We DO NOT store or share third party data or information<br/>Thank you!';
$lang['socialTab_Form_desc_foursquare'] = 'Click or tap "Authorize Foursquare" to authenticate and allow activation on your W-Address page. We DO NOT store or share third party data or information<br/>Thank you!';


/*Step Tab lable*/
$lang['step_lable_myaccount'] = 'My Account';
$lang['step_lable_paymentinfo'] = 'Payment Info';
$lang['step_lable_businessDetails'] = 'W-Address Details';
$lang['step_lable_socialconn'] = 'Social Connections';
$lang['step_lable_wowdeal'] = 'WOW Deal';

//Extra Languahe Varibale
$lang['msg_popupHeading'] 		 = "Choose Your W-Address";
$lang['msg_avl'] 		 		 = "Available";
$lang['msg_nonavl'] 		     = "Unavailable try again";
$lang['number_not_published'] 		     = "Number not published";


$lang['text_for_id_lable'] 		     = "ID,Label or Name to display on page";
$lang['text_for_typeinfo'] 		     = "Type info to display on page";
$lang['text_for_areacode_phone'] 		     = "(Area code) Phone number";
$lang['text_for_Upload_JPG_GIF_PNG_images_only'] 		     = "Upload JPG, GIF or PNG images only.Also use Upload to replace current image";


//$lang['msg_commercial_myaccount_myplab_Tab'] 		     = "";

/*Payment view page lang param*/
$lang['page_register'] = "Registration form for new user";
$lang['no_social_found'] = "Sorry These type of social is not added in your accout";
$lang['payment_done'] = "Thank you your payment was successful";
$lang['payment_error'] = "Sorry Something is going wrong Please Try again.....!";
$lang['payment_upgrade'] = "Your Plan is successfully Upgrade";
$lang['search_error'] = "W-Address not active,please try again";
$lang['search_error_empty'] = "please enter W-Address to connect";
$lang['page_title'] = "Admin Panel - ".$config['site_name'];
$lang['login_welcome_text'] = $config['site_name']." - Admin Panel";
$lang['login_alert_text'] = "Please login with your Username and Password.";
$lang['forgot_password_text'] = "Please enter your email.";
$lang['forgot_password_error'] = "Please enter correct email.";
$lang['forgot_password_record_not_found'] = "Oops..Subscribers name is not found in system..";
$lang['forgot_password_welcome_text'] = "Password Recovery Process";
$lang['welcome'] = "Welcome";
$lang['finish'] = "Finish";
$lang['logout'] = "Logout";
$lang['edit_profile'] = "Edit Profile";
$lang['visit_site'] = "Visit Site";
$lang['login_to_admin'] = "Login to admin panel";

#ORDER PARAMERTES STARTS
$lang['order_number'] = "Order No.";
$lang['order_date'] = "Order Date";
$lang['subs_date'] = "Subscription Date";
$lang['expiry_date'] = "Expiry Date";
$lang['transaction_status']="Tran# Status";
$lang['subscription_status']="Subscription Status";
$lang['payment_status']="Payment Status";
$lang['transaction_number'] = "Tran#";
$lang['user_email'] = "User Email";
$lang['content_title'] = "Content Title";
$lang['paypal_token'] = "Paypal Token";
$lang['paid_amount'] = "Paid Amount";
$lang['payment_type'] = "Payment Type";
$lang['order_description'] = "Order Description";
$lang['edit_order'] = "Edit order";
$lang['orders'] = "Orders";
#ORDER PARAMERTES ENDS

#ORDER SUBSCRIPTION STARTS
$lang['subscriptions'] = "Subscriptions";

#ORDER SUBSCRIPTION ENDS

$lang['message_title'] = "Message title";
$lang['message'] = "Message";
$lang['send'] = "Send";
$lang['new_password'] = "New password";
$lang['old_password'] = "Old password";
$lang['confirm_password'] = "Confirm password"; //added to v1.6
$lang['leave_password'] = "Leave the field blank if you do not want to change the password."; //added to v1.6
$lang['login'] = "Login";
$lang['back'] = "Back";
$lang['confirm_delete'] = "Do you really want to delete this item?";
$lang['confirm_delete_selected'] = "Do you really want to delete selected items?";
$lang['access_denied'] = "Incorrect  Username or password.";
$lang['verify_mail_error'] = "Please verify the email id first and then try to login";
$lang['login_user_not_exist'] = "The username you provided does not exist";
$lang['password_char_error'] = "Passwords can be no longer than 32 chracters";
$lang['incorrect_password'] = "You specified an incorrect password";
$lang['restricted_access'] = "You have no privileges to access this page.";
$lang['go_back'] = "Back to previous page";
$lang['filter'] = "Filter";
$lang['title'] = "Title";
$lang['url'] = "URL";
$lang['content'] = "Content";
$lang['language'] = "Language";
$lang['edit'] = "Edit";
$lang['delete'] = "Delete";
$lang['seo_panel_link'] = "Search engine optimization";
$lang['seo_browser_title'] = "Pages title";
$lang['seo_h1_title'] = "H1 title";
$lang['meta_keywords'] = "Meta keywords";
$lang['meta_description'] = "Meta description";
$lang['advanced_panel_link'] = "Advanced options";
$lang['template'] = "Template";
$lang['save'] = "Save";
$lang['status'] = "Status";
$lang['published'] = "Published";
$lang['date'] = "Created";
$lang['image'] = "Image";
$lang['images_panel_link'] = "Images";
$lang['short_text'] = "Short text";
$lang['long_text'] = "Long text";
$lang['user_group'] = "User group";
$lang['username_exist'] = "Choosen username is already taken. Please choose another one.";
$lang['identifier'] = "Identifier"; 
$lang['change_order'] = "Change order";
$lang['delete_selected'] = "Delete selected";
$lang['target'] = "Open blank";
$lang['slug_error'] = "The same URL is already in use! URL has to be unique.";
$lang['success_insert'] = "Successfully added!";
$lang['success_edit'] = "Successfully updated!";
$lang['success_email_password'] = "Your login credentials sent to your email successfully!";
$lang['success_delete'] = "Successfully deleted!";
$lang['js_disabled'] = "It seems that your web browser has the JavaScript support disabled.<br />If you want to be able to use this system, please enable the JavaScript support.";
$lang['change_order_help'] = "Use the drop & drag method to change the menu order. To create nested items just drop one item into another.";
$lang['use_existing_images'] = 'Use existing images';
$lang['use_existing_images_help'] = 'Add the name of the image you want to use. For example <em><b>image.jpg</b></em>. Images must be available in the <em><b>/uploads/gallery/</b></em> directory.';
$lang['image_desc'] = 'Image description';
$lang['featured'] = 'Featured';
$lang['category'] = 'Category';
$lang['categories'] = 'Categories';
$lang['parent_category'] = 'Parent category'; // added to 1.6
$lang['id'] = 'ID'; // added to 1.7
#Menus Title Starts
$lang['menu'] = "Menus";
$lang['dashboard_menu'] = "Dashboard";
$lang['users_menu'] = "Users";
$lang['orders_menu'] = "Orders";
$lang['subscriptions_menu'] = "Subscriptions";
$lang['cms_menu'] = "CMS";
$lang['settings_menu'] = "Settings";
$lang['account_settings'] = "Account Settings";
$lang['plans_menu'] = "Plans";
$lang['plantype_menu'] = "Plan Type";
#Menus Title Ends
#Menu functionlity starts
$lang['view_menus'] = "View menus";
$lang['add_menu'] = "Add menu";
$lang['edit_menu'] = "Edit menu";
$lang['view_menu_items'] = "View menu items";
#Menu functionlity ends
//User Profile Page
$lang['user_profile'] = "User Profile";
$lang['cms'] = "CMS";
$lang['pages'] = "Pages";
$lang['view_pages'] = "View pages";
$lang['add_page'] = "Add page";
$lang['page_options'] = "Page Options";
$lang['edit_page'] = "Edit page";
//Tab - User Edit Page
$lang['tab_personal_details'] = "Personal Details";
$lang['tab_business_details'] = "Business Details";
$lang['tab_connected_to'] = "Connected To";
$lang['tab_deal'] = "Deal";
$lang['tab_subscription'] = "Subscription";
$lang['elements'] = "Editable elements";
$lang['add_element'] = "Add editable element";
$lang['edit_element'] = "Edit editable element";
$lang['labels'] = "Labels";
$lang['view_labels'] = "View labels";
$lang['add_label'] = "Add label";
$lang['edit_label'] = "Edit label";
$lang['news'] = "News";
$lang['view_news'] = "View news";
$lang['add_news'] = "Add news";
$lang['edit_news'] = "Edit news";
$lang['gallery'] = "Photo gallery";
$lang['add_gallery'] = "Add gallery";
$lang['edit_gallery'] = "Edit gallery";
$lang['articles'] = "Articles"; // added to 1.6
$lang['view_articles'] = "View articles"; // added to 1.6
$lang['add_article'] = "Add article"; // added to 1.6
$lang['edit_article'] = "Edit article"; // added to 1.6
$lang['add_category'] = "Add category"; // added to 1.6
$lang['view_categories'] = "View categories"; // added to 1.6
$lang['plans'] = "Plans";
$lang['plan_title'] = "Plan Title";
$lang['price'] = "Price";
$lang['currency'] = "Currency";
$lang['plan_type_name'] = "Plan Type Name";
$lang['duration_number'] = "Plan Period Months";
$lang['add_plans'] = "Add Plans";
$lang['edit_plans'] = "Edit Plans";
$lang['plan_type'] = "Plan Type";
$lang['plan_type_name'] = "Plan Name";
$lang['plan_status'] = "Status";
$lang['add_plan_type'] = "Add Plan Type";
$lang['edit_plan_type'] = "Edit Plan Type";
#USER DETAILS
$lang['username'] = "Nick Name";
$lang['password'] = "Password";
$lang['name'] = "Name";
$lang['first_name'] = "First Name";
$lang['last_name'] = "Last Name";
$lang['fax'] = "Fax";
$lang['postal_code'] = "Postal Code";
$lang['anniversary'] = "Anniversay";
$lang['device_type'] = "Device Type";
$lang['recv_prod_info'] = "Received Product Info";
$lang['web_address'] = "Web Address";
$lang['email'] = "Email";
$lang['company'] = "Company";
$lang['country'] = "Country";
$lang['state'] = "State";
$lang['phone'] = "Phone No.";
$lang['created'] = "Created On";
$lang['last_login_date']="Last login";
$lang['company'] = "Company";
$lang['user_ip'] = "User IP Address";
$lang['dob'] = "Date of Birth";
$lang['address1'] = "Address1";
$lang['address2'] = "Address 2";
$lang['State'] = "State";
$lang['plan_type'] = "Plan type";
$lang['w_address'] = "w_address";
$lang['profile_pic'] = "Profile Image";
$lang['business_pic'] = "Business Image";
$lang['logo_pic'] = "Logo or Brand";
$lang['intro_text'] = "Intro Text";
$lang['area_code'] = "Area code";
$lang['city'] = "city";
$lang['zipcode'] = "zipcode";
$lang['describe_deal'] = "Describe the Deal";
$lang['success_business_edit'] = "Business Details Successfully updated!";
$lang['success_personal_edit'] = "Personal Details Successfully updated!";
$lang['success_deals_edit'] = "Deals Successfully updated!";
$lang['subcription_date'] = "Subcription Date";
#USER FUNCTIONLITY
$lang['users'] = "Users";
$lang['view_users'] = "View users";
$lang['add_user'] = "Add user";
$lang['edit_user'] = "Edit user";

#SETTINGS FUNCTIONLITY
$lang['key'] = "Key";
$lang['value'] = "Value";
$lang['label'] = "Label";



