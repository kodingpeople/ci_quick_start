<?php

class Nic
{
    var $CI;
    var $_email;
    var $_password;

    var $_table = array(
                    'users' => 'users',
					'waddresses' => 'waddresses'
                    );

    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
        $this->CI->load->helper('string');
		$this->CI->load->helper('cookie');
	}

    function Nic()
    {
        self::__construct();
    }

    function restrict($restrict_to = NULL, $redirect_to = NULL)
	{
		$redirect_to = ($redirect_to == NULL) ? $this->CI->config->item('base_url') . "user" : $redirect_to;
		if ($restrict_to !== NULL) {
			if ($this->logged_in() == TRUE) {
				
                if ($this->CI->session->userdata('waddress_role_id') >= $restrict_to) {
					return TRUE;
				} else {
					show_error("You do not have sufficient rights to access this page!");
					//redirect($redirect_to);	
				}
			} else {
				//show_error("You do not have sufficient rights to access this page!");
				redirect($redirect_to);	
			}
		} else {
			show_error("You do not have sufficient rights to access this page!");
		}
	}

    function restrict_social()
    {
       if ($this->logged_in() == TRUE) {
                $this->CI->db->select('plan_id');
                $query = $this->CI->db->get_where($this->_table['users'], array('id' => $this->CI->session->userdata('waddress_user_id')), 1);
               
			    $this->CI->db->select('social_count');
                $query2 = $this->CI->db->get_where($this->_table['waddresses'], array('id' => $this->CI->session->userdata('current_waddress_id')), 1);
               
			    $_UsersocialCount = $query2->row()->social_count;
                $_planName = $query->row()->plan_id;
                $_PlanLimitVar = trim($_planName).'_plan_slimit';
                $_planLimit = $this->CI->config->item($_PlanLimitVar);
                
                $nextstep ='';
                   
                if($_UsersocialCount >= $_planLimit){
                            
                    if($this->CI->session->userdata('social_from_page') == 'manageaddress'){

                         if($this->CI->session->userdata('waddress_user_plan_id') == 'commercial'){
                             $nextstep = 'dealTab';
                             $this->CI->session->set_flashdata('info', "Sorry...You can't add more Social !"); 
                             redirect($this->CI->config->item('base_url').'manageaddress?tabindex=d&currentstep='.$nextstep);
                         }  
                        else{

                             $this->CI->session->set_flashdata('info', "Sorry...You can't add more Social !"); 
                             $this->CI->session->set_userdata('from_url', 'login'); 
                             redirect($this->CI->config->item('base_url').'manageaddress?tabindex=s');

                        }  

                    }
                    else{

                        if($this->CI->session->userdata('waddress_user_plan_id') == 'commercial'){
                             $nextstep = 'dealTab';
                             $this->CI->session->set_flashdata('info', "Sorry...You can't add more Social !"); 
                             redirect($this->CI->config->item('base_url').'paymentview?tabindex=d&currentstep='.$nextstep);
                         }  
                        else{

                             $this->CI->session->set_flashdata('info', "Sorry...You can't add more Social !"); 
                             $this->CI->session->set_userdata('from_url', 'login'); 
                             redirect($this->CI->config->item('base_url').'paymentview?tabindex=s');

                        }  
                    }
                }
        }
    }

	function username_exists( $username )
	{
		$this->CI->db->select('nick_name');
		$query = $this->CI->db->get_where($this->_table['users'], array('nick_name' => $username), 1);
	
		if ($query->num_rows() !== 1) {
			return FALSE;
		} else {
			$this->_username = $username;
			return TRUE;
		}
	}

    function email_exists( $email )
    {
        $this->CI->db->select('email');
        $query = $this->CI->db->get_where($this->_table['users'], array('email' => $email), 1);
    
        if ($query->num_rows() !== 1) {
            return FALSE;
        } else {
            $this->_email = $email;
            return TRUE;
        }
    }

     function subscribersName_exists( $subscribersName )
    {
        $this->CI->db->select('last_name');
        $query = $this->CI->db->get_where($this->_table['users'], array('last_name' => $subscribersName), 1);
    
        if ($query->num_rows() !== 1) {
            return FALSE;
        } else {
            $this->_email = $email;
            return TRUE;
        }
    }
        
    function forgot_password($email, $subscribersname, $redirect_to = NULL, $error_view = NULL)
	{	
        $this->CI->load->library('Email');
		$query = $this->CI->db->get_where('users', array(
                                                            'email' => $email,
                                                            'last_name' => $subscribersname,
                                                            'role_id' => '2'
                                                           ), 1
                                                 );

		if ($query->num_rows() === 1) {
			$row = $query->row();
                        $new_password= $this->generateStrongPassword();
                        $data=array('password'=>$this->encrypt($new_password));
                        
                        /*Update password starts*/
                        $query=$this->CI->db->set($data)
                                        ->where('role_id','2')
                                        ->where('email',$email)
                                        ->where('last_name',$subscribersname)
                                        ->update('users');
                        /*Update password ends*/
                        
                        /*Send newly generated password to USERS email*/
                        $data['row'] = $row;
                        $data['new_password'] = $new_password;
                        $this->CI->load->library('email');
                        $message = $this->CI->load->view('mail/forgotpass',$data,TRUE); 
                        $this->CI->email->initialize(array('mailtype' => 'html'));
                        $this->CI->email->from($this->CI->config->item('email_from'), 'Waddress');
                        $this->CI->email->to($row->email);
                        $this->CI->email->subject('Waddress PasswordRecovery request');
                        $this->CI->email->message($message);
                        $this->CI->email->send();
                       
                        //send_email
                        $this->CI->session->set_flashdata('success',$this->CI->lang->line('success_email_password'));
                        redirect($redirect_to);
                        
		} else {
                        $this->CI->session->set_flashdata('error',$this->CI->lang->line('forgot_password_record_not_found'));
			redirect($redirect_to);
		}
	}

	function check_password( $password )
	{
		$this->CI->db->select('password');
		$query = $this->CI->db->where($this->_table['users'], array('nick_name' => $this->_username), 1)->row();
	
		if ($query->password == $this->encrypt($password)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

    function check_string_length( $string )
    {
        $string = trim($string);
        return strlen($string);
    }

    function encrypt( $data )
    {
        if ($this->CI->config->item('encryption_key') !== NULL) {
            return sha1($this->CI->config->item('encryption_key').$data);
        } else {
            show_error('Please set an encryption key in your config file. <a href="javascript:history.back();">back</a>');
        }
    }
	
function login($subscribername, $password, $rememberMe, $redirect_to = NULL, $error_view = NULL)
{		
		 $query = $this->CI->db->get_where('users', 
										array(
											'last_name' => $subscribername,
											'password' => $this->encrypt($password),
                                            'role_id' => '2'
                                            //'status' => '1'
											), 1
									  );
		
		if ($query->num_rows() === 1) 
		{		
			$row = $query->row();
			
			if($row->status == '1')
			{
				$data = array(
								'waddress_logged_in' => TRUE,
								'waddress_sess_expire_on_close' => TRUE,
								'waddress_username' => $row->last_name,
								'waddress_user_id' => $row->id,
								'waddress_user_plan_id' => $row->plan_id,
								'waddress_name' => $row->last_name,
								'from_url' => 'login'
								
							  );
				$this->CI->session->set_userdata($data);
				$cookie = array(
					'name'   => 'waddress_cifm',
					'value'  => md5('fm_pass'),
					'prefix' => 'ci_',
					'expire' => '0',
					'path'   => '/'
				);	
				set_cookie($cookie);
			   // SET USER COOKIE EMAIL AND PASSWORD 
				if($rememberMe !='')
				{ 
				   $cookieUserEmail = array(
						'name'   => 'userEmail',
						'value'  => $subscribername,
						'expire' => time()+3600
					);
	
					$cookiePassword = array(
						'name'   => 'userPassword',
						'value'  => $password,
						'expire' => time()+3600
						
					);
				
					set_cookie($cookieUserEmail);
					set_cookie($cookiePassword);
				}
				else
				{  
					$cookieUserEmail = array(
						'name'   => 'userEmail',
						'value'  => '',
						'expire' => time()-3600
					);
	
					$cookiePassword = array(
						'name'   => 'userPassword',
						'value'  => '',
						'expire' => time()-3600
						
					);
				
					set_cookie($cookieUserEmail);
					set_cookie($cookiePassword);
	
	
				}
				$this->CI->session->set_flashdata('info', 'successfully logged in ');    
				redirect($redirect_to);
						
			}
			else{
				if ($error_view != NULL) {
					$data['error'] = $this->CI->lang->line('verify_mail_error');
					$this->CI->load->view($error_view, $data);
					$this->CI->load->view('home');
					$this->CI->load->view('footer');
	
				} else {
					redirect($redirect_to);
				}
			}
		}
		else {
			if ($error_view != NULL) {
				$data['error'] = $this->CI->lang->line('access_denied');
				$this->CI->load->view($error_view, $data);
                $this->CI->load->view('home');
                $this->CI->load->view('footer');

			} else {
				redirect($redirect_to);
			}
		}
	}
    function autologin($subscribersName, $password, $redirect_to = NULL, $error_view = NULL)
    {       
        $query = $this->CI->db->get_where('users', 
                                        array(
                                            'last_name' => $subscribersName,
                                            'password' => $password,
                                            'role_id' => '2',
                                            'status' => '1'

                                            ), 1
                                      );
            
       if ($query->num_rows() === 1) {
                        
            $row = $query->row();
            $data = array(
                            'waddress_logged_in' => TRUE,
                            'waddress_sess_expire_on_close' => TRUE,
                            'waddress_username' => $row->last_name,
                            'waddress_user_id' => $row->id,
                            'waddress_user_plan_id' => $row->plan_id,
                            'waddress_name' => $row->first_name,
                            'from_url' => 'signup'
                            
                          );
            $this->CI->session->set_userdata($data);
            
            /*check it is comes form mail paypal link or from register*/
            if(isset($_REQUEST['type'])){
                if($_REQUEST['type'] == 'Upgrade') {
                    $this->CI->session->set_userdata('from_url', 'login'); 
                }   
            }


            $cookie = array(
                'name'   => 'waddress_cifm',
                'value'  => md5('fm_pass'),
                'prefix' => 'ci_',
                'expire' => '0',
                'path'   => '/'
            );  
            set_cookie($cookie);

           // SET USER COOKIE EMAIL AND PASSWORD 
           
            if($rememberMe !='')
            { 
               $cookieUserEmail = array(
                    'name'   => 'userEmail',
                    'value'  => $subscribersName,
                    'expire' => time()+3600
                );

                $cookiePassword = array(
                    'name'   => 'userPassword',
                    'value'  => $password,
                    'expire' => time()+3600
                    
                );
            
                set_cookie($cookieUserEmail);
                set_cookie($cookiePassword);
            }
            else
            {  
                $cookieUserEmail = array(
                    'name'   => 'userEmail',
                    'value'  => '',
                    'expire' => time()-3600
                );

                $cookiePassword = array(
                    'name'   => 'userPassword',
                    'value'  => '',
                    'expire' => time()-3600
                    
                );
            
                set_cookie($cookieUserEmail);
                set_cookie($cookiePassword);


            }
            $this->CI->session->set_flashdata('info', 'successfully logged in ');    
            redirect($redirect_to);
        } else {
            if ($error_view != NULL) {
                $data['error'] = $this->CI->lang->line('access_denied');
                $this->CI->load->view($error_view, $data);
                $this->CI->load->view('home');
                $this->CI->load->view('footer');

            } else {
                redirect($redirect_to);
            }
        }
    }

    function logged_in()
    {
        return $this->CI->session->userdata('waddress_logged_in');
    }

    function is_login($id='')
    {
        return $id;
    }

    function logout($redirect_to = NULL)
    {
		$this->CI->session->sess_destroy();
		delete_cookie('waddress_cifm');
		if ($redirect_to != NULL) {
			redirect($redirect_to);	
		}
    }
    
    // Generates a strong password of N length containing at least one lower case letter,
    // one uppercase letter, one digit, and one special character. The remaining characters
    // in the password are chosen at random from those four sets.
    //
    // The available characters in each set are user friendly - there are no ambiguous
    // characters such as i, l, 1, o, 0, etc. This, coupled with the $add_dashes option,
    // makes it much easier for users to manually type or speak their passwords.
    //
    // Note: the $add_dashes option will increase the length of the password by
    // floor(sqrt(N)) characters.

    function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds')
    {
    $sets = array();
    if(strpos($available_sets, 'l') !== false)
    $sets[] = 'abcdefghjkmnpqrstuvwxyz';
    if(strpos($available_sets, 'u') !== false)
    $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
    if(strpos($available_sets, 'd') !== false)
    $sets[] = '23456789';
    if(strpos($available_sets, 's') !== false)
    $sets[] = '!@#$%&*?';

    $all = '';
    $password = '';
    foreach($sets as $set)
    {
    $password .= $set[array_rand(str_split($set))];
    $all .= $set;
    }

    $all = str_split($all);
    for($i = 0; $i < $length - count($sets); $i++)
    $password .= $all[array_rand($all)];

    $password = str_shuffle($password);

    if(!$add_dashes)
    return $password;

    $dash_len = floor(sqrt($length));
    $dash_str = '';
    while(strlen($password) > $dash_len)
    {
    $dash_str .= substr($password, 0, $dash_len) . '-';
    $password = substr($password, $dash_len);
    }
    $dash_str .= $password;
    return $dash_str;
    }
	
}

/* End of file Auth.php */
/* Location: ./application/libraries/Auth.php */