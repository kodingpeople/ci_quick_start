<?php
class Commonmodel extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper("file");

	}
	
	function deletebrandLogo($table,$id,$imageName){

		$data['brand_logo']    =  '';
        $this->db->set($data);
        $this->db->where ('id', $id);
        $this->db->update($table);
        
        //Unlink image from folder
        $imagePath = $this->config->item('image_directory').'user/'.$imageName;
   		delete_files($imagePath);
   	}

	/* Return All Record of perticular Table*/
	function get($table, $per_page = '', $segment = '')
	{
		$this->db->order_by('id desc');
		if($per_page && $segment) $this->db->limit($per_page, $segment);
		return $this->db->get($table)->result();
	}
	
	/* Return Active Users */
	function getActiveUsers($table, $per_page = '', $segment = '')
	{
		$this->db->order_by('id desc');
		if($per_page && $segment) $this->db->limit($per_page, $segment);
		return $this->db->get_where($table, array('status' => '1'))->result();
	}


	/*Get userwise W-address*/
	function getWaddress_by_userId($id)
	{
		//$this->db->order_by('id desc');
		$this->db->select('*');
		return $this->db->get_where('waddresses', array('user_id' => $id))->result();
	}

	/* Get Setting Records*/
	function get_settings($table)	
	{
		$this->db->order_by('id desc');
		
		$settings_row = $this->db->get($table)->result();
		echo '<pre>';
		print_r($settings_row);
		exit;
	}
	
	/* Get Items by Language */
	function get_items_by_language($table)
	{
		$this->db->order_by('id', 'desc');
		return $this->db->get_where($table, array('lang' => $this->config->item('language_abbr')))->result();		
	}
	
	/* Get Cms Pages by slug */
	function check_slug($table, $slug, $lang)
	{
		$this->db->select('slug');
		return $this->db->get_where($table, array('slug' => $slug, 'lang' => $lang))->num_rows();
	}
	
	/* Get cms slug by Id*/
	function get_slug_by_id($table, $id) {
		$this->db->select('slug');
		return $this->db->get_where($table, array('id' => $id))->row()->slug;
	}	
	
	/* Get items by perticular Id - Single Record*/
	function get_item_by_id($table, $id)
	{
                
		return $this->db->get_where($table, array('id' => $id))->row();
             
	}

	/*  Get item by perticular User id */
	function get_item_by_uid($table, $id)
	{
                
		return $this->db->get_where($table, array('user_id' => $id))->row();
             
	}

	/*  Get item by perticular W-address id */
	function get_item_by_wid($table, $wid)
	{
                
		return $this->db->get_where($table, array('waddress_id' => $wid))->row();
             
	}
	
	/*  Get Social Count by User id */
	function get_usersocialcount_by_id($table, $id)
	{
                
		return $this->db->get_where($table, array('id' => $id))->row()->social_count;
             
	}
	
	/* Get item by title */
	function get_item_by_title($table, $title)
	{
		return $this->db->get_where($table, array('title' => $title))->row();
	}	
	
	/* Get item by sulg*/
	function get_item_by_slug($table, $slug)
	{
		return $this->db->get_where($table, array('slug' => $slug))->row();
	}	
	
	/* Insert Data into Table */
	function insert($table, $data)
	{
		$this->db->set($data);
		$this->db->insert($table);
	}
	
	/* Update Data in to Table */
	function update($table, $id, $data)
	{
		$this->db->update($table, $data, array('id'=>$id));
	}
	
	/* Update by User id */
	function updatebyuid($table, $id, $data)
	{
		$this->db->update($table, $data, array('user_id'=>$id));
	}
	
	/* Update users Social by waddress id */
	function social_update($table, $id, $data ,$social_type)
	{
		$this->db->update($table, $data, array('waddress_id'=>$id,'social_type'=>$social_type));
	}

	/* Update social by id - Waddress Table */
	function update_social_count($id){

		$this->db->set('social_count', 'social_count + 1', FALSE);
		$this->db->where('id',$id);
		$this->db->update('waddresses');
	}

	/* Delete social */
	function delete_social($id,$socialtype){

		$this->db->set('social_count', 'social_count - 1', FALSE);
		$this->db->where('id',$id);
		$this->db->update('waddresses');
		$this->db->where(array('waddress_id' => $id, 'social_type' => $socialtype))->delete('socials');

	}
	
	function delete($table, $id, $module = FALSE, $module_id = FALSE)
	{
		$this->db->where('id', $id)->delete($table);
	}
	
	/* Delete Business Details by userId */
	function delete_business_by_id($table, $id, $module = FALSE, $module_id = FALSE)
	{
		$this->db->where('user_id', $id)->delete($table);
	}

	function get_images($id, $module, $limit = 999, $offset = 0)
	{
		$this->db->select('id, name, date, time, title, text, position');
		$this->db->order_by('position', 'asc');
		$this->db->order_by('date', 'desc');
		return $this->db->get_where('images', array('item_id' => $id, 'module' => $module), $limit, $offset)->result();
	}
	
	function save_image($image, $item_id, $module, $title = '', $text = '', $description = '', $status = 1, $date, $time)
	{
		$this->db->select_max('position');
		$position = $this->db->get_where('images', array('item_id' => $item_id, 'module' => $module))->row()->position;
		$data = array(
					  'module' => $module,
					  'item_id' => $item_id,
					  'name' => $image,
					  'date' => date('Y-m-d'),
					  'time' => date('H:i:s'),
					  'title' => $title,
					  'text' => $text,
					  'status' => $status,
					  'position' => $position + 1
					  );
		$this->db->insert('images', $data);
	}
	
	function delete_images($item_id, $module)
	{
		$this->db->where(array('item_id' => $item_id, 'module' => $module))->delete('images');
	}
	
	function delete_image($id, $module)
	{
		$this->db->where('id', $id)->delete('images');
	}
	
	function encrypt($data)
	{
		 return sha1($this->config->item('encryption_key').$data);
	}	

	function dashborad_details(){
                
        $dashborad=array();
       	$dashboard['totalUsers']=$this->db->where('status','1')->count_all_results('users');
        $dashboard['totalPlans']=$this->db->where('status','1')->count_all_results('plans');
        $dashboard['totalCmsPages']=$this->db->where('status','1')->count_all_results('cms');
        $dashboard['totalSocialsettings']=$this->db->count_all_results('settings');
        
	    return $dashboard;
	}

	function waddress_count($user_id){
		return $this->db->where('user_id',$user_id)->count_all_results('waddresses');

	}
	function get_business_by_userId($table, $user_id)
	{
                
		return $this->db->get_where($table, array('user_id' => $user_id))->row();
             
	}
	function get_business_by_wId($table, $wId)
	{
                
		return $this->db->get_where($table, array('waddress_id' => $wId))->row();
             
	}
	function get_deal_by_userId($table, $user_id)
	{
                
		return $this->db->get_where($table, array('user_id' => $user_id))->row();
             
	}
	function get_connected_to_by_userId($table, $user_id)
	{
                
		return $this->db->get_where($table, array('user_id' => $user_id))->result();
             
	}
	/*function get_connected_to_by_wId($table, $wid)
	{
                
		return $this->db->get_where($table, array('waddress_id' => $wid))->result();
             
	}*/
	function get_connected_to_by_wId($table, $wId)
	{
                
		return $this->db->get_where($table, array('waddress_id' => $wId))->result();
             
	}
	function get_business_by_userId_isAffected($table, $user_id,$wid)
 	{
             
	   $query = $this->db->get_where($table, array('user_id' => $user_id,'waddress_id' => $wid),1);
	     
	        if ($query->num_rows() !== 1) {
	         
	            return FALSE;
	        } else {
	            return TRUE;
	        }
             
 	}

	function checkwaddressexits($waddress){

		//$this->db->select('waddresses');
        $query = $this->db->get_where('waddresses', array('w_address' => $waddress, 'status' => '1'),1);
    	
        if ($query->num_rows() !== 1) {
        	
            return FALSE;
        } else {
            return TRUE;
        }
	}

	function getexpiry_date($months_period){
	    $today = time();
	    $LaterTime = strtotime($months_period, $today);
	    return $expiry_date = date('Y-m-d', $LaterTime);
	}

	function update_waddress(){

		$waddress 			= $this->input->post('waddress');
		$data['w_address'] 	= $waddress;
		$data['user_id'] 	= $this->session->userdata('waddress_user_id');
		//$data['status'] 	= '1';
        //set expiry date for social commerce and personal user type
        $planId  = $this->session->userdata('waddress_user_plan_id');
        if($planId == 'social_commerce'){
        	$months_period = $this->config->item('social_commerce_period');
        	$data['expiry_date'] = $this->getexpiry_date($months_period);  	
        	$data['subcription_date'] =  date('Y-m-d'); 	
        }else if($planId == 'personal'){
        	$months_period = '+12 months';
        	$data['expiry_date'] = $this->getexpiry_date($months_period);  	
        	$data['subcription_date'] =  date('Y-m-d'); 	
        }

        $data['created'] 	=  date('Y-m-d'); 
		$this->insert('waddresses',$data);
		$wid = $this->db->insert_id();
		
		//set waddress in session varible
		$waddressData = $this->get_item_by_id('waddresses',$wid);
		$this->session->set_userdata('current_waddress', $waddressData->w_address);
		$this->session->set_userdata('current_waddress_id', $waddressData->id);
		
		
		
	}

	

	function get_social_exit($type)
	{
        $id = $this->session->userdata('waddress_user_id');  
		$wid = $this->session->userdata('current_waddress_id');       
		$query = $this->db->get_where('socials', array('social_type'=>$type,'waddress_id' => $wid),1);
       	if ($query->num_rows() == 1) {
        	return TRUE;
        } else {
            return FALSE;
        }      
	}
/**
 * Display the difference between two dates
 * (30 years, 9 months, 25 days, 21 hours, 33 minutes, 3 seconds)
 *
 * @param  string $start starting date
 * @param  string $end=false ending date
 * @return string formatted date difference
 */
function dateDiff($start,$end=false){
   $return = array();
   
   try {
      $start = new DateTime($start);
      $end = new DateTime($end);
      $form = $start->diff($end);
   } catch (Exception $e){
      return $e->getMessage();
   }
   
   $display = array('y'=>'year',
               'm'=>'month',
               'd'=>'day',
               'h'=>'hour',
               'i'=>'minute',
               's'=>'second');
   foreach($display as $key => $value){
      if($form->$key > 0){
         $return[] = $form->$key.' '.($form->$key > 1 ? $value.'s' : $value);
      }
   }
   return implode($return, ', ');
}

function get_page_Id($table, $id){
        return $this->db->get_where($table, array('id' => $id))->row();
    }
	


	


	

        
}
?>