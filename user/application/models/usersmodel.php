<?php
class Usersmodel extends CI_Model
{
	private $_usersTable    = 'users';
    private $_businessTable = 'business_details';
    private $_plansTable    = 'plans';
    private $_dealsTable    = 'deals';
    private $_paymentTable  = 'payments';
    private $_waddressTable = 'waddresses';

function __construct(){

      parent::__construct();
      $this->load->library('Email');
      $this->load->library('encrypt');
      $this->load->helper('date');
}


function get_groups($role = 0){
	//select groups without developer
      $this->db->select('id, title, description');
      $role = $this->session->userdata('group_id');
      if ($role == 4) {
         return $this->db->get_where($this->_roleTable, array('id >' => 2))->result();	
     } else {
         return $this->db->get_where($this->_roleTable, array('id' => 3))->result();	
     }
 }

function get_all_groups(){
//select groups without developer
  $this->db->select('id, title, description');
  return $this->db->get($this->_roleTable)->result();
}

function check_username($username){
  $this->db->select('username');
  return $this->db->get_where($this->_usersTable, array('username' => $username))->num_rows();
}

function getlastwId($id){

    $this->db->from('waddresses');
    $this->db->where('user_id',$id);
    $this->db->order_by("id", "desc");
    $query = $this->db->get(); 
    return $query->row();
}

function getfirstwId($id){

    $this->db->from('waddresses');
    $this->db->where('user_id',$id);
    $this->db->order_by("id", "ASC");
    $query = $this->db->get(); 
    return $query->row();
}

function encrypt_password($data){
 return sha1($this->config->item('encryption_key').$data);
}

function get_user_password_by_id($id){
  $this->db->select('password');
  return $this->db->get_where($this->_usersTable, array('id' => $id))->row()->password;
}

function get_user_password_by_email($email){
    $this->db->select('password');
    return $this->db->get_where($this->_usersTable, array('email' => $email))->row()->password;
}

function get_user_password_by_subscribersName($subscribersName){
    $this->db->select('password');
    return $this->db->get_where($this->_usersTable, array('last_name' => $subscribersName))->row()->password;
}

function get_email_by_uid($id){
    $this->db->select('email');
    return $this->db->get_where($this->_usersTable, array('id' => $id))->row()->email;
}    
function get_item_by_username($username){
  $this->db->select('id, username, name');
  return $this->db->get_where($this->_usersTable, array('username' => $username))->row();
}

function get_fullname_by_id($uid){
  $this->db->select('first_name, last_name');
  return $this->db->get_where($this->_usersTable, array('id' => $uid))->row();
}

function get_item_by_id($id){
  $this->db->select('id, username, name');
  return $this->db->get_where($this->_usersTable, array('id' => $id))->row();
}

function GetDatabysid($sid){
    $this->db->select('*');
    $this->db->from('payments');
    $this->db->join('users', 'users.id = payments.user_id');
    $this->db->where('payments.subscription_id', $sid);
    return $this->db->get()->row();
}

function get_status_by_email($email){
    $this->db->select('status');
    return $this->db->get_where($this->_usersTable, array('email' => $email))->row()->status;
}

function get_status_by_subscribersName($subscribersName){
    $this->db->select('status');
    return $this->db->get_where($this->_usersTable, array('last_name' => $subscribersName))->row()->status;
}

function updatestatus_by_email($email){
    $data = array('status' => '1');
    $this->db->set($data);
    $this->db->where('email', $email);
    $this->db->update($this->_usersTable);
}

function updatestatus_by_subscribersName($subscribersName){
    $data = array('status' => '1');
    $this->db->set($data);
    $this->db->where('last_name', $subscribersName);
    $this->db->update($this->_usersTable);
}

function save($id) {

    if($this->input->post('city') != '')
        $city = $this->input->post('city');
    else
        $city = '';    

    if($this->input->post('state') != '')
        $state = $this->input->post('state');
    else
        $state = '';         


    if($this->input->post('paymentType') != '')
        $paymentType = $this->input->post('paymentType');
    else
        $paymentType = 'm';       

    $data = array(
        'email'         =>  $this->input->post('email'),
        'first_name'    =>  $this->input->post('first_name'),
        'last_name'     =>  $this->input->post('last_name'),
        'area_code'     =>  $this->input->post('area_code'),
        'phone'         =>  $this->input->post('phone'),
        'city'          =>  $city,
        'state'         =>  $state,
        'zipcode'       =>  $this->input->post('zipcode'),
        'status'        =>  $this->input->post('status'), 
        'created'       =>  date('y-m-d H:i:s'),
        'role_id'       => '2',
        'plan_id'       =>  $this->input->post('userplan'),
        'status'        =>  '0',
        'payment_type'  =>  $paymentType,

        );
    if($this->input->post('userplan') == 'personal'){
            //get time period 
        $months_period = '+12 months';
        $data['expiry_date'] = $this->getexpiry_date($months_period);  

    }
    elseif($this->input->post('userplan') == 'social_commerce'){

       $months_period = $this->config->item('social_commerce_period');
       $data['expiry_date'] = $this->getexpiry_date($months_period);  

   }
   else{  
       $months_period = $this->config->item('commercial_period');
       $data['expiry_date'] = $this->getexpiry_date($months_period);       

   }   

   if ($id == FALSE) {
            //$data['w_address']   =  $this->input->post('w_address');
    $today = date('Y-m-d');
    $data['subcription_date'] = $today;     
    $data['password']    =  $this->encrypt_password($this->input->post('password'));


    $this->db->set($data);
    $this->db->insert($this->_usersTable);

    /* send activation mail */
           /*$query = $this->db->get_where('users', array(    'email' => $this->input->post('email'),
                                                            'role_id' => '2',
                                                            'status' => '0'

                                                        ), 1
                                        );
                if ($query->num_rows() === 1) {
                            $row = $query->row();
                            /*Send email
                            $verifyUrl = base_url('users/verifyidentity/?key='.base64_encode($row->email));
                            $this->email->from($this->config->item('email_from'), 'Waddress');
                            $this->email->to($row->email);
                            $this->email->subject('Waddress Team:Welcome to Our site');
                            $this->email->message('"Hi,
                                                    '.$row->first_name.'
                                                    You have been notified that your successfully Register in Our site
        
                                                    Email Id : '.$row->email.' 
                                                    but you cant login before verify your identity for verify identity click <a href="'.$verifyUrl.'">here</a> 

                                                    Thank you for using our services!

                                                    With Regards
                                                    Waddress Team');

$this->email->send();*/
                            // echo $this->email->print_debugger();
                            //exit;
                //}
    } else {

     $this->db->set($data);
     $this->db->where('id', $id);
     $this->db->update($this->_usersTable);
    }
}  

/**
     *
     * Method for Save Business Details
     *
     * @param       $action  (add,update)
     * @return      
     * @author      Bhavesh Khanpara
     * 
     */ 
 
function save_business_details($action){
    
     /*Phone published here*/
        if(@$this->input->post('business_phone_publish') == 'on'){
            $phone_publish = '1';
        }else{
            $phone_publish = '0';   
        }

    /* chk_active */
    if(@$this->input->post('chk_active') == 'on'){
        $chk_active = '1';
    }else{
        $chk_active = '0';   
    }    

    $data = array(
        'company_name'    =>  $this->input->post('business_company_name'),
        'email'           =>  $this->input->post('business_email'),
        'web_address'     =>  $this->input->post('business_web_address'),
        'intro_text'      =>  $this->input->post('business_intro_text'),
        'address_1'       =>  $this->input->post('business_address_1'),
        'city'            =>  $this->input->post('business_city'),
        'state'           =>  $this->input->post('business_state'),
        'zipcode'         =>  $this->input->post('business_zipcode'), 
        'area_code'       =>  $this->input->post('business_area_code'),
        'phone'           =>  $this->input->post('business_phone'),
        'phone_publish'   =>  $phone_publish,
        'modified'        =>  date('Y-m-d H:i:s')
        );
    //business Image
    if($this->input->post('business_banner_photo') != ''){
        $data['business_pic']    =  $this->input->post('business_banner_photo');
    }
    //brand or logo  
    if($this->input->post('brand_logo') != "")  {
       $data['brand_logo']   =  $this->input->post('brand_logo');
    }
   if(isset($_FILES['Filedata'])){
		if($_FILES['Filedata'] != ''){
			$fileName = $this->imageUpload();
			  if($fileName){
			  	$data['business_pic']    =  $fileName;
			  }
		}
	}
	
	if(isset($_FILES['Filedata_business_logo'])){
		if($_FILES['Filedata_business_logo'] != ''){
			$fileNameLogo = $this->imagelogoUpload();
			  if($fileNameLogo){
			  	$data['brand_logo']    =  $fileNameLogo;
			  }
		}
	}
   $id = $this->session->userdata('waddress_user_id');
   $wid = $this->session->userdata('current_waddress_id');
   
    if($action == 'add'){
         $data['user_id'] = $id;
         $data['waddress_id'] = $wid ;
         $data['created'] = date('Y-m-d H:i:s');
         $this->db->set($data);
         $this->db->insert($this->_businessTable);
    }else {
        //print_r($data);
       // exit;
        $this->db->set($data);
        $this->db->where ('waddress_id', $wid);
		$this->db->update($this->_businessTable);
        //$this->db->where('user_id', $wid);
        //echo $this->db->last_query();
        //exit();
    }

        $data_status = array( 'status' =>  $chk_active );
        $this->db->update($this->_waddressTable, $data_status, array('id'=>$wid));
}

    

	 /**
     *
     * Method for Change Password
     *
     * @param       old_password,new_password  
     * @return      status
     * @author      Bhavesh Khanpara
     * 
     */ 
     function changePassword()
     {   
        $this->db->select('id');
        $this->db->where('email',$this->session->userdata('waddress_username'));
        $this->db->where('password',$this->encrypt_password($this->input->post('old_password')));
        $query=$this->db->get('users');   
        if ($query->num_rows() > 0)
        {
            $row = $query->row();
            if($row->id === $this->session->userdata('waddress_username'))
            {
                $data = array(
                  'password' => $this->encrypt_password($this->input->post('new_password'))
                  );
                $this->db->where('email',$this->session->userdata('waddress_username'));
                $this->db->where('password',$this->encrypt_password($this->input->post('old_password')));
                if($this->db->update('users', $data)) 
                {
                 return "done";
             }else{
                return "not_changed";
            }
        }else{
            return "not_changed";
        }


    }else{
        return "Wrong_Old_Password";
    }

}  


    /**
     *
     * Return Waddress suggestion
     *
     * @param       user id   
     * @return      array
     * @author      Bhavesh Khanpara
     * 
     */
    
    function getwaddress_suggestion() {

        $this->load->helper('string');
        /*generate waddress suggestion array*/    
        $userData = $this->db->get_where('users',array('id'=>$this->session->userdata('waddress_user_id')))->row();
        /*Made a array*/
        $fullname                   = array($userData->first_name,$userData->last_name);
        $firstname                  = array($userData->first_name);
        $lastname                   = array($userData->last_name);
        $lastname_first_character   = array(substr($userData->last_name, 0, 1));
        $firstname_first_character  = array(substr($userData->first_name, 0, 1));
        $first_and_last_charter     = array(substr($userData->first_name, 0, 1),substr($userData->last_name, 0, 1));
        $explodeEmail               = explode('@',$userData->email);
        $email_array                = array($explodeEmail[0]);
        $special                    = array('.','_');

        /* Make rand array*/
        $final_array = array();
            $final_array[] = $firstname[array_rand($firstname)].$special[array_rand($special)].$lastname[array_rand($lastname)]; //Khanpara_Bhavesh,Khanpara.Bhavesh
            $final_array[] = $lastname[array_rand($lastname)].$special[array_rand($special)].$firstname[array_rand($firstname)];//.Bhavesh19;
            $final_array[] = $firstname[array_rand($firstname)].random_string('numeric', 2);
            $final_array[] = $firstname[array_rand($firstname)].random_string('numeric', 2).$special[array_rand($special)].$lastname_first_character[array_rand($lastname_first_character)];//Bhavesh06.k
            $final_array[] = $lastname[array_rand($lastname)].$special[array_rand($special)].$firstname_first_character[array_rand($firstname_first_character)].random_string('numeric', 2); 
			// Khanpara.B43
            $final_array[] = $first_and_last_charter[array_rand($first_and_last_charter)].$special[array_rand($special)].$fullname[array_rand($fullname)].random_string('numeric', 2);  //B_Khanpara17
            $final_array[] = $firstname[array_rand($firstname)].$lastname[array_rand($lastname)].$special[array_rand($special)].$first_and_last_charter[array_rand($first_and_last_charter)].random_string('numeric', 2); //K.B92
            $final_array[] = $email_array[array_rand($email_array)].$special[array_rand($special)].$first_and_last_charter[array_rand($first_and_last_charter)].random_string('numeric', 2); //bhavesh.Khanpara_Z99
            
            $final         = array_unique($final_array);
            shuffle($final);

            /* Get all exits avalible waddress array */
            $newGenreated_waddress = $final;
            $getexits_waddress     = $this->getallwaddress();

        //delete common element from array
            $return_array_diff = array_diff($newGenreated_waddress, $getexits_waddress);

            $output =  array_slice($return_array_diff, 0, 5); 
            return $output;
        } 

     /**
     *
     * Get all Exitsting waddress from database
     *
     * @param          
     * @return      array
     * @author      Bhavesh Khanpara
     * 
     */
     function getallwaddress(){

        $this->db->select('w_address');
        $waddresses = $this->db->get($this->_usersTable)->result_array();
        $new_array = array();
        foreach ($waddresses as $key => $value) {

         $new_array[] = $value['w_address'];
     }

     return $new_array;
 }   
 public function save_paymentview_personal_details(){
    $id = $this->session->userdata('waddress_user_id');
    $wid = $this->session->userdata('current_waddress_id');
   /*Email public code stuff here*/
   
        if(@$this->input->post('email_publish') == 'on'){
            $email_publish = '1';
        }else{
            $email_publish = '0';   
        }
        /*Phone published here*/
        if(@$this->input->post('phone_publish') == 'on'){
            $phone_publish = '1';
        }else{
            $phone_publish = '0';   
        }

   if($this->session->userdata('waddress_user_plan_id') != 'commercial')
   {

    /*Phone published here*/
        if(@$this->input->post('chk_active') == 'on'){
            $chk_active = '1';
        }else{
            $chk_active = '0';   
        }
    //Varible for manage W-address satus 
        $data_status = array( 'status' =>  $chk_active );
        $this->db->update($this->_waddressTable, $data_status, array('id'=>$wid));
    
    $data = array(
        
        'email'         =>  $this->input->post('email'),
        'email_publish' =>  $email_publish,
        // 'first_name'    =>  $this->input->post('first_name'),
        // 'last_name'     =>  $this->input->post('last_name'),
        'intro_text'    =>  $this->input->post('welmessage'),
        'area_code'     =>  $this->input->post('area_code'),
        'phone'         =>  $this->input->post('phone'),
        'phone_publish' =>  $phone_publish,
        'city'          =>  $this->input->post('city'),
        'state'         =>  $this->input->post('state'),
        'zipcode'       =>  $this->input->post('zipcode'),
        'address'       =>  $this->input->post('address'),

        );
    } else{
        $data = array( 'email_publish' =>  $email_publish );
    }
	

		

    if($this->input->post('signupImage') != ''){
        $data['profile_pic']    =  $this->input->post('signupImage');
    }
	
	if(isset($_FILES['Filedata'])){
		if($_FILES['Filedata'] != ''){
			$fileName = $this->imageUpload();
			  if($fileName){
			  	$data['profile_pic']    =  $fileName;
			  }
		}
	}
	
    if($this->input->post('password') != ''){
         $data['password']       =  $this->encrypt_password($this->input->post('password'));
    }
	
	 $this->db->set($data);
	 $this->db->where('id', $id);
	 $this->db->update($this->_usersTable);

}
public function imageUpload(){
	
			header( " HTTP/1.0 200 OK" );
			header("HTTP/1.0 200 OK");
			header("Content-Type: text/html"); 
			header('Access-Control-Allow-Origin: *');
			
			$fileName = '';
			/* upload user profile pic */
			if(!empty($_FILES['Filedata']) && !empty($_FILES['Filedata']['name'])){
				$fileName = $_FILES['Filedata']['name'];
				$fileTempName = $_FILES['Filedata']['tmp_name'];
				$whitelist = array('jpg', 'jpeg', 'JPG', 'JPEG', 'png','gif');
				$fileParts  = pathinfo($_FILES['Filedata']['name']);

				$path_original = '../upload/user/';
				$path_thumb = $path_original.'thumb/';

				if (in_array($fileParts['extension'],$whitelist)) {

                    //@mkdir($path_original, 0777);
					$fileName = time().'.'.$fileParts['extension'];
					if(move_uploaded_file($fileTempName, $path_original.$fileName))
					{
						chmod($path_original.$fileName, 0777);
					}
                    /*if(move_uploaded_file($fileTempName, $path_thumb.$fileName))
                    {
                        chmod($path_thumb.$fileName, 0777);
                    }
                    */return  $fileName;
					exit;

                } else {
                	return 0;
					exit;
                }
            }       
        }
		
public function imagelogoUpload(){
	
			header( " HTTP/1.0 200 OK" );
			header("HTTP/1.0 200 OK");
			header("Content-Type: text/html"); 
			header('Access-Control-Allow-Origin: *');
			
			$fileName2 = '';
			/* upload user profile pic */
			if(!empty($_FILES['Filedata_business_logo']) && !empty($_FILES['Filedata_business_logo']['name'])){
				$fileName2 = $_FILES['Filedata_business_logo']['name'];
				$fileTempName = $_FILES['Filedata_business_logo']['tmp_name'];
				$whitelist = array('jpg', 'jpeg', 'JPG', 'JPEG', 'png','gif');
				$fileParts  = pathinfo($_FILES['Filedata_business_logo']['name']);

				$path_original = '../upload/user/';
				$path_thumb = $path_original.'thumb/';

				if (in_array($fileParts['extension'],$whitelist)) {

                    //@mkdir($path_original, 0777);
					$fileName2 = 'logo'.time().'.'.$fileParts['extension'];
					if(move_uploaded_file($fileTempName, $path_original.$fileName2))
					{
						chmod($path_original.$fileName2, 0777);
					}
                    /*if(move_uploaded_file($fileTempName, $path_thumb.$fileName))
                    {
                        chmod($path_thumb.$fileName, 0777);
                    }
                    */return  $fileName2;
					exit;

                } else {
                	return 0;
					exit;
                }
            }       
        }
function getexpiry_date($months_period){
    $today = time();
    $LaterTime = strtotime($months_period, $today);
    return $expiry_date = date('Y-m-d', $LaterTime);
}

function getDiffrentDays($Today,$Expiry){
  $daylen = 60*60*24;
		/* $Today = '2010-03-29';
        $Expiry = '2009-07-16';*/
		//return (strtotime($Expiry)-strtotime($Today))/$daylen;
        $day1 = new DateTime($Today);
        $day2 = new DateTime($Expiry);
        $interval = round(abs($day2->format('U') - $day1->format('U')) / (60*60*24));
        return $interval;
    }
    
function delete_account(){
        $id = $this->session->userdata('waddress_user_id');

        $this->db->where(array('id' => $id))->delete('users');
        $this->db->where(array('user_id' => $id))->delete('socials');
        $this->db->where(array('user_id' => $id))->delete('payments');
        $this->db->where(array('user_id' => $id))->delete('waddresses');
        $this->db->where(array('user_id' => $id))->delete('business_details');
       /*
         $udata = array(
          'status'    =>  '0',
          'w_address' => '',
          );
        $this->db->set($udata);
        $this->db->where('id', $id);
        $this->db->update($this->_usersTable); */
  }

function delete_individual_waddress($wid,$userId){
    if($userId != NULL){
        $this->db->where(array('id' => $userId))->delete('users');
    }
    
    $this->db->where(array('waddress_id' => $wid))->delete('socials');
    $this->db->where(array('waddress_id' => $wid))->delete('payments');
    $this->db->where(array('id' => $wid))->delete('waddresses');
    $this->db->where(array('waddress_id' => $wid))->delete('business_details');
    $this->db->where(array('waddress_id' => $wid))->delete('deals');



}
function get_waddress_json_array() {
      $term  = $this->input->get_post('term');
      $query = mysql_query("SELECT * FROM waddresses where w_address like '".$term."%' AND status = '1' order by id");
      $json  = array();
      while($_WaddressData = mysql_fetch_array($query)){
        $_userData = $this->commonmodel->get_item_by_id('users', $_WaddressData["user_id"]);	
        if(!empty($_userData)){

          if($_userData->plan_id != 'commercial' && $_WaddressData["status"] == '1'){
           $json[]=array('value'=> $_WaddressData["w_address"],'label'=>$_WaddressData["w_address"]);
           }
           else{
              if($this->Payment_done_before_waddress($_WaddressData["id"]) == TRUE && $_WaddressData["status"] == '1')
              {
                $json[]=array('value'=> $_WaddressData["w_address"],'label'=>$_WaddressData["w_address"]);
              }
        }
      }
    }
    return json_encode($json);
}

function get_waddress_json_array_backup() {
  $term=$_GET["term"];
  $query=mysql_query("SELECT * FROM waddresses where w_address like '".$term."%' AND status = '1' order by id ");
  $json=array();
  while($users=mysql_fetch_array($query)){
     if($users['plan_id'] != 'commercial'){
        if($users["w_address"] != ''){
           $json[]=array('value'=> $users["w_address"],'label'=>$users["w_address"]);
       }
   }else{
       if($this->Payment_done_before($users['id']) == TRUE && $users["w_address"] != ''){
          $json[]=array('value'=> $users["w_address"],'label'=>$users["w_address"]);

      }
  }
}
return json_encode($json);
}
function get_payment_status() {


  if($this->get_userPayment_exit() == TRUE){
    //$uid = $this->session->userdata('waddress_user_id');
    $wid = $this->session->userdata('current_waddress_id');
    $this->db->select('payment_status');
    return $this->db->get_where($this->_paymentTable, array('waddress_id' => $wid))->row()->payment_status;
    }
    else{
       return FALSE;
    }
}

function getipnCounter($uid){
 return $this->db->get_where($this->_paymentTable, array('user_id' => $uid))->row()->ipn_count;
}


function get_userPayment_exit() {
    //$uid = $this->session->userdata('waddress_user_id');      
    $wid = $this->session->userdata('current_waddress_id');      
    $query = $this->db->get_where('payments', array('waddress_id' => $wid),1);
    if ($query->num_rows() == 1) {
        return TRUE;
    } else {
        return FALSE;
    }      
}

function Payment_done_before($uid){
    $query = $this->db->get_where('payments', array('user_id' => $uid),1);
    if ($query->num_rows() == 1) {
        return TRUE;
    } else {
        return FALSE;
    }      
}
function Payment_done_before_waddress($waddressId){
    $query = $this->db->get_where('payments', array('waddress_id' => $waddressId),1);
    if ($query->num_rows() == 1) {
        return TRUE;
    } else {
        return FALSE;
    }      
}


function Payment_done_before_per_waddress($uid,$wid){
    $query = $this->db->get_where('payments', array('user_id' => $uid,'waddress_id'=>$wid),1);
    if ($query->num_rows() == 1) {
        return TRUE;
    } else {
        return FALSE;
    }      
}

function do_payment()
{   

        /*$_userId = $this->session->userData('waddress_user_id');
        $_subscription_id = $_REQUEST['subscr_id'];  
        $_player_email = $_REQUEST['payer_email'];  
        $_plan_id = $_REQUEST['custom']; 
        if   (   $_plan_id == 2) {   $_amount = $_REQUEST['amount1'];    }
        else {                       $_amount = $_REQUEST['amount3'];    }
        $data = array(
                        'user_id'           =>  $_userId,
                        'plan_id'           =>  $_plan_id,
                        'subscription_id' =>  $_subscription_id,
                        'price'    =>  $_amount,
                        'payment_status'     => 'done',
                        'paypal_email'     => $_player_email,
                        'created' => date('Y-m-d')
                    );

        if($_REQUEST['payer_status'] == 'verified'){
            if($this->get_userPayment_exit() == TRUE){
                $this->db->set($data);
                $this->db->where('user_id', $_userId);
                $this->db->update($this->_paymentTable);
                
                //Set expiry Date
                if($_plan_id == 2){
                    $months_period = $this->config->item('social_commerce_period');
                    $data_timeperiod['expiry_date'] = $this->getexpiry_date($months_period);  
                }
                else{  
                    $months_period = $this->config->item('commercial_period');
                    $data_timeperiod['expiry_date'] = $this->getexpiry_date($months_period);       
                }
                $data_timeperiod['subcription_date'] = date('Y-m-d');
                $this->db->set($data_timeperiod);
                $this->db->where('id',$_userId);
                $this->db->update('users');


            }
            else
            {   
                //Set expiry Date
                if($_plan_id == 2){
                    $months_period = $this->config->item('social_commerce_period');
                    $data_timeperiod['expiry_date'] = $this->getexpiry_date($months_period);  
                }
                else{  
                    $months_period = $this->config->item('commercial_period');
                    $data_timeperiod['expiry_date'] = $this->getexpiry_date($months_period);       
                }
                $data_timeperiod['subcription_date'] = date('Y-m-d');
                $this->db->set($data_timeperiod);
                $this->db->where('id',$_userId);
                $this->db->update('users');
                //inssert
                 $this->db->set($data);
                 $this->db->insert($this->_paymentTable);  
                 
            }
            return TRUE;

        }
        else{
            return FALSE;
        }   */

        return TRUE;
    }

function getWaddressInfo($waddress){

    return $this->db->get_where('waddresses', array('w_address' => $waddress,'status' => '1'))->row();

}


function get_userinfo_by_waddress($table, $_userId){

  return $this->db->get_where($table, array('id' => $_userId))->row();
}

function save_deal_details(){

  /* chk_active */
    /*if(@$this->input->post('chk_active') == 'on'){
        $chk_active = '1';
    }else{
        $chk_active = '0';   
    }*/

  $_userId = $this->session->userData('waddress_user_id');
  $wid = $this->session->userdata('current_waddress_id');
  $_dealDesc  = $this->input->post('txt_deal');

  $data = array(
      'description' =>  $_dealDesc,
      'created' => date('Y-m-d')
      );
  $query = $this->db->get_where($this->_dealsTable, array('waddress_id' => $wid),1);

  if ($query->num_rows() == '1') {
      $this->db->set($data);
      $this->db->where('waddress_id', $wid);
      $this->db->update($this->_dealsTable); 

      //Varible for manage W-address satus 
       /* $data_status = array( 'status' =>  $chk_active );
        $this->db->update($this->_waddressTable, $data_status, array('id'=>$wid));*/

  }
  else{

   $data['user_id'] = $_userId;
   $data['waddress_id'] = $wid;
   $this->db->set($data);
   $this->db->insert($this->_dealsTable); 

   //Varible for manage W-address satus 
        //$data_status = array( 'status' =>  $chk_active );
        //$this->db->update($this->_waddressTable, $data_status, array('id'=>$wid));

    }
}


function upgradePlan(){

    $_userId = $this->session->userData('waddress_user_id');
    $_subscription_id = $_REQUEST['subscr_id'];  
    $_player_email = $_REQUEST['payer_email'];  
    $_plan_id = $_REQUEST['custom']; 

    if   ($_plan_id == 2) {$_amount = $_REQUEST['amount1'];}
    else  { $_amount = $_REQUEST['amount3'];}


    $data = array(
        'user_id'           =>  $_userId,
        'plan_id'           =>  $_plan_id,
        'subscription_id'   =>  $_subscription_id,
        'price'             =>  $_amount,
        'payment_status'    => 'done',
        'paypal_email'      => $_player_email,
        'created'  => date('Y-m-d')
        );

    if($_REQUEST['payer_status'] == 'verified'){
        if($this->get_userPayment_exit() == TRUE){
            $this->db->set($data);
            $this->db->where('user_id', $_userId);
            $this->db->update($this->_paymentTable);

                //Set expiry Date
            if($_plan_id == 2){
                $months_period = $this->config->item('social_commerce_period');
                $data_timeperiod['expiry_date'] = $this->getexpiry_date($months_period);  
                $data_timeperiod['plan_id'] = 'social_commerce';  
            }
            else{  
                $months_period = $this->config->item('commercial_period');
                $data_timeperiod['expiry_date'] = $this->getexpiry_date($months_period);
                $data_timeperiod['plan_id'] = 'commercial';        
            }
            $data_timeperiod['subcription_date'] = date('Y-m-d');
            $this->db->set($data_timeperiod);
            $this->db->where('id',$_userId);
            $this->db->update('users');


        }
        else
        {   
                //Set expiry Date
            if($_plan_id == 2){
                $months_period = $this->config->item('social_commerce_period');
                $data_timeperiod['expiry_date'] = $this->getexpiry_date($months_period); 
                $data_timeperiod['plan_id'] = 'social_commerce'; 
            }
            else{  
                $months_period = $this->config->item('commercial_period');
                $data_timeperiod['expiry_date'] = $this->getexpiry_date($months_period);   
                $data_timeperiod['plan_id'] = 'commercial';       
            }
            $data_timeperiod['subcription_date'] = date('Y-m-d');
            $this->db->set($data_timeperiod);
            $this->db->where('id',$_userId);
            $this->db->update('users');
                //inssert
            $this->db->set($data);
            $this->db->insert($this->_paymentTable);  

        }
        return TRUE;

    }
    else{
        return FALSE;
    }   

}
function get_waddress_dropdown(){
    $this->db->from($this->_waddressTable);
    $this->db->order_by('id');
    $this->db->where('user_id',$this->session->userdata('waddress_user_id'));
    $result = $this->db->get();
    $return = array();
    if($result->num_rows() > 0){
        $return[''] = 'please select W-Address';
        foreach($result->result_array() as $row){
            $return[$row['id']] = $row['w_address'];
        }
    }
    return $return;
}

function get_waddress_defaultdropdownId($id){
    $this->db->select('id');
    $this->db->from($this->_waddressTable);
    $this->db->order_by('id');
    $this->db->where('user_id',$id);
    $result = $this->db->get()->row();
    $return = array();
      if($result->id > 0)
          return $result->id;
      else
          return 0;
     
}



}
?>