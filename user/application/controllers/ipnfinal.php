<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
======Please do not change plan_id because here we consider plan as per plan_id and business logic
depends on plan_Id.
=> Controller for Manage Payment for Waddress Users 
=> plam_id - 1 //Free Personal User
=> Plan_id - 2 //Social Commerce users
=> plam_id-  3 //Commercial user
*/
class Ipnfinal extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('paypal_lib');
	}

	function index()
	{

	
		$this->load->library('session');
		$this->load->model('commonmodel');
		$this->load->model('usersmodel');
		$this->load->library('email');
		$this->load->helper(array('form', 'url'));
	
		$raw_post_data = file_get_contents('php://input');
		$raw_post_array = explode('&', $raw_post_data);
		$myPost = array();
		$collumns = array();
		$colvalues = array();
		foreach ($raw_post_array as $keyval)
		{
			$keyval = explode ('=', $keyval);
			if (count($keyval) == 2)
			{
				$myPost[$keyval[0]] = urldecode($keyval[1]);
				array_push($collumns, $keyval[0]);
				array_push($colvalues, "'".mysql_real_escape_string(html_entity_decode(urldecode($keyval[1])))."'");
			}
	
		}
		// read the post from PayPal system and add 'cmd'
		$req = 'cmd=_notify-validate&';
	
	
		$req .= $raw_post_data;
	
	
		if(isset($_POST["ipn_track_id"]) && $_POST["ipn_track_id"] != '')
		{
			$paypalurl = "https://www.sandbox.paypal.com/cgi-bin/webscr";   
		   
		   // STEP 2: Post IPN data back to paypal to validate
	
			$ch = curl_init($paypalurl);
			curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
			curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
	
			// In wamp like environments that do not come bundled with root authority certificates,
			// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
			// of the certificate as shown below.
			// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
			if( !($res = curl_exec($ch)) ) {
				error_log("Got " . curl_error($ch) . " when processing IPN data");
				curl_close($ch);
				exit;
			}
			curl_close($ch);
			 
	
			// STEP 3: Inspect IPN validation result and act accordingly
	
			if (strcmp ($res, "VERIFIED") == 0) {
					 @$json          	= json_decode($_REQUEST['custom']);
					 @$planId           = $json->planId;
					 @$waddressId       = $json->waddressId;
					 @$uid              = $json->userId;
					 @$plan_period      = $json->plan_period;
					 @$_subscription_id = $_REQUEST['subscr_id'];  
					 @$_player_email    = $_REQUEST['payer_email']; 
					 //@$customerName     = $_REQUEST['last_name']; 
					 @$payment_currency = $_REQUEST['mc_currency']; 
					 @$txn_id           = $_REQUEST['txn_id']; 
					 @$payment_status 	= $_REQUEST['payment_status'];
					 //mail('bhavesh.khanpara@indianic.com', 'test', print_r($json,TRUE));	
					// mail('bhavesh.khanpara@indianic.com', 'waddressId', print_r($waddressId,TRUE));	
					  //== 'Completed';
					 @$_player_email2 = $this->usersmodel->get_email_by_uid($uid);
					 @$getDataObj = $this->usersmodel->get_fullname_by_id($uid);
					 //@$customerName     = @$getDataObj->first_name.' '.@$getDataObj->last_name; 
					 @$customerName     = @$getDataObj->last_name; 
					/*Update Table payment and user when user select plan id 2 and do free 2 months 
					Method for Autorecurring Process Stuff here*/
					if($_REQUEST['txn_type'] == 'subscr_signup' && ($planId == '2' || $planId == 2))
					{	
							$payment_amount = $_REQUEST['amount1'];
							$data = array(
								'user_id'           =>  $uid,
								'plan_id'           =>  $planId,
								'subscription_id'   =>  $_subscription_id,
								'price'             =>  $payment_amount,
								'payment_status'    =>  'Completed',
								'paypal_email'      =>  $_player_email,
								'created' 			=>  date('Y-m-d')
							);
							$this->db->set($data);
							$this->db->insert('payments');
	
							$months_period = $this->config->item('social_commerce_period');
							
							$data_timeperiod['expiry_date'] = $this->usersmodel->getexpiry_date($months_period); 
							$data_timeperiod['subcription_date'] = date('Y-m-d');
							$this->db->set($data_timeperiod);
							$this->db->where('id',$uid);
							$this->db->update('users');
	
							//increment counter in every call
							$this->db->set('ipn_count', 'ipn_count + 1', FALSE);
							$this->db->where('user_id',$uid);
							$this->db->update('payments');
							
							//Send mail
								
							$data['subscription_id'] =  $_subscription_id;
							$data['customerName'] =  $customerName;
							
							$this->load->library('email');
							$message = $this->load->view('mail/subscription',$data,TRUE); 
							$this->email->initialize(array('mailtype' => 'html'));
							$this->email->from($this->config->item('base_email'), 'Social Commerces Plan');
							$this->email->to($_player_email2); 
							$this->email->subject("User $uid - Thank you for Subscribe Social Commerces Plan");             
							$this->email->message($message);
							$this->email->send();
							
					}
					//Recurring 2nd call or payment is pay for method 3 - auto recurring stff here
					else if($_REQUEST['txn_type'] == 'subscr_payment')
					{
					
						if($planId == '2' || $planId == 2) {
							
							$counter = $this->usersmodel->getipnCounter($uid);
							if($counter == '1' || $counter == 1)
							{
							   
								/*Update Payment Table*/
								$payment_amount = $_POST['mc_gross'];
								$data = array(
									'plan_id'           =>  $planId,
									'subscription_id' =>  $_subscription_id,
									'price'    =>  $payment_amount,
									'payment_status'     => $payment_status,
									'paypal_email'     => $_player_email,
									'created' => date('Y-m-d')
								);
								
									$this->db->set($data);
									$this->db->where('user_id',$uid);
									$this->db->update('payments');
								
								
								/*Update User Table*/
								//add 4 months 
								$months_period = $this->config->item('social_commerce_period_2');
								if($payment_status == 'Completed'){
									$data_timeperiod['expiry_date'] = $this->usersmodel->getexpiry_date($months_period); 
									//$data_timeperiod['subcription_date'] = date('Y-m-d');
								}
								
								
								$this->db->set($data_timeperiod);
								$this->db->where('id',$uid);
								$this->db->update('users');
								
								//increment counter in every call
								$this->db->set('ipn_count', 'ipn_count + 1', FALSE);
								$this->db->where('user_id',$uid);
								$this->db->update('payments');
	
								//Send mail
								$data['subscription_id'] =  $_subscription_id;
								$data['customerName'] =  $customerName;
								
								$this->load->library('email');
								$message = $this->load->view('mail/subscription',$data,TRUE); 
								$this->email->initialize(array('mailtype' => 'html'));
								$this->email->from($this->config->item('base_email'), 'Auto Upgrade Social Commerces Plan');
								$this->email->to($_player_email2); 
								$this->email->subject("User $uid - Your Social Commerces Plan will be auto subscribe for next 4 months");		                            $this->email->message($message);
								$this->email->send();
								
	
							}else{
								//add 6 months 
								/*Update Payment Table*/
								$payment_amount = $_POST['mc_gross'];
									$data = array(
									'plan_id'           =>  $planId,
									'subscription_id'   =>  $_subscription_id,
									'price'             =>  $payment_amount,
									'payment_status'    =>  $payment_status,
									'paypal_email'      =>  $_player_email,
									'created'           => date('Y-m-d')
									);
	
								$this->db->set($data);
								$this->db->where('user_id',$uid);
								$this->db->update('payments');
	
								/*Update User Table*/
								$months_period = $this->config->item('social_commerce_period_3');
								if($payment_status == 'Completed'){
									$data_timeperiod['expiry_date'] = $this->usersmodel->getexpiry_date($months_period); 
								}
								else{
									 //$data_timeperiod['status'] = '0';
									}
								//$data_timeperiod['subcription_date'] = date('Y-m-d');
								$this->db->set($data_timeperiod);
								$this->db->where('id',$uid);
								$this->db->update('users');
								
								//increment counter in every call
								$this->db->set('ipn_count', 'ipn_count + 1', FALSE);
								$this->db->where('user_id',$uid);
								$this->db->update('payments');
	
	
								//Send mail
								$data['subscription_id'] =  $_subscription_id;
								$data['customerName'] =  $customerName;
								
								$this->load->library('email');
								$message = $this->load->view('mail/subscription',$data,TRUE); 
								$this->email->initialize(array('mailtype' => 'html'));
								$this->email->from($this->config->item('base_email'), 'Auto Upgrade Social Commerces Plan');
								$this->email->to($_player_email2); 
								$this->email->subject("User $uid - Your Social Commerces Plan will be auto subscribe for next 6 months");	                            $this->email->message($message);
								$this->email->send();
								}
							
								//increment counter in every call
								$this->db->set('ipn_count', 'ipn_count + 1', FALSE);
								$this->db->where('user_id',$uid);
								$this->db->update('payments');  
						}
						else if($planId == '3' || $planId == 3){
								
							$payment_amount = $_POST['mc_gross'];
									$data = array(
									'plan_id'           =>  $planId,
									'subscription_id'   =>  $_subscription_id,
									'price'             =>  $payment_amount,
									'payment_status'    =>  $payment_status,
									'paypal_email'      =>  $_player_email,
									'created'           =>  date('Y-m-d')
									);
	
							if($this->usersmodel->Payment_done_before($uid) == TRUE)
							{
								//update
								$this->db->set($data);
								$this->db->where('user_id',$uid);
								$this->db->update('payments');
	
								/*Update User Table*/
								$months_period = $this->config->item('commercial_period');
								if($payment_status == 'Completed'){
									$data_timeperiod['expiry_date'] = $this->usersmodel->getexpiry_date($months_period); 
								}else{
									// $data_timeperiod['status'] = '0';
									}
								//$data_timeperiod['subcription_date'] = date('Y-m-d');
								
								$this->db->set($data_timeperiod);
								$this->db->where('id',$uid);
								$this->db->update('users');
								
								//increment counter in every call
								$this->db->set('ipn_count', 'ipn_count + 1', FALSE);
								$this->db->where('user_id',$uid);
								$this->db->update('payments');
	
								//Send mail
								$data['new_text'] =  'Congratulations, your annual Commercial W-address subscription is active';
								$data['subscription_id'] =  $_subscription_id;
								$data['customerName'] =  $customerName;
							    $data['plan_desc'] = 'This is to notify that you are sucessfully subscribed to commercial subscription plan.';
								$this->load->library('email');
								$message = $this->load->view('mail/subscription',$data,TRUE); 
								$this->email->initialize(array('mailtype' => 'html'));
								$this->email->from($this->config->item('base_email'), 'Auto Upgrade Commercial Plan');
								$this->email->to($_player_email2); 
								$this->email->subject("1 year Commercial Plan auto subscribed");						
								$this->email->message($message);
								$this->email->send();
							
	
							}       
							else{
								
								//insert Data in Payment Table
								$data['user_id'] = $uid;
								//$data['waddress_id'] = $waddressId;
								$this->db->set($data);
								$this->db->insert('payments');
	
								/*Update User Table*/
								$months_period = $this->config->item('commercial_period');
								if($payment_status == 'Completed'){
									$data_timeperiod['expiry_date'] = $this->usersmodel->getexpiry_date($months_period); 
								}else{
									 //$data_timeperiod['status'] = '0';
									}
								$data_timeperiod['subcription_date'] = date('Y-m-d');
								$this->db->set($data_timeperiod);
								$this->db->where('id',$uid);
								$this->db->update('users');
								
								//increment counter in every call
								$this->db->set('ipn_count', 'ipn_count + 1', FALSE);
								$this->db->where('user_id',$uid);
								$this->db->update('payments');
								
								
								$data['subscription_id'] =  $_subscription_id;
								$data['new_text'] =  'Congratulations, your annual Commercial W-address subscription is active';
								$data['customerName'] =  $customerName;
								$data['plan_desc'] = 'This is to notify that you are sucessfully subscribed to commercial subscription plan.';
								
								$this->load->library('email');
								$message = $this->load->view('mail/subscription',$data,TRUE); 
								$this->email->initialize(array('mailtype' => 'html'));
								$this->email->from($this->config->item('base_email'), 'Commercial Plan');
								$this->email->to($_player_email2); 
								$this->email->subject("1 year Commercial Plan auto subscribed");				                            
								$this->email->message($message);
								$this->email->send();
								
							}
						
						}
						//Send Mail if payment fail
						if($payment_status != 'Completed'){
								/*$this->db->set('status', '0');
								$this->db->where('id',$uid);
								$this->db->update('users');
								*/
								$this->email->from($this->config->item('admin_email'), 'PAYMENT FAIL');
								$this->email->to($this->config->item('mail_from')); 
								$this->email->subject("$uid - PAYMENT FAIL");
								$this->email->message('You are inactive by Our system dur to Auto subscription Issue.');	
								$this->email->send();
						}
				   
						
						
						}
					/*Method for Sample Paypal form Paymant - Manually Payment Process Stuff here*/
					else if($_REQUEST['txn_type'] == 'web_accept')
					{	
						$payment_amount = $_POST['mc_gross'];
						if($planId == '2' || $planId == 2)
						{
							
							$data = array(
									'plan_id'           =>  $planId,
									'transaction_token' =>  $txn_id,
									'price'             =>  $payment_amount,
									'payment_status'    =>  $payment_status,
									'paypal_email'      =>  $_player_email,
									'created' 			=>  date('Y-m-d')
								);
							
							if($this->usersmodel->Payment_done_before_waddress($waddressId) == TRUE)
							{	
								//$data['waddress_id'] = $waddressId;
								//update
								$this->db->set($data);
								$this->db->where('waddress_id',$waddressId);
								$this->db->update('payments');
								
								/*Update User Table*/
								if($plan_period == '1' || $plan_period == 1){ 
								$months_period = $this->config->item('social_commerce_period_4');
								} 
								else if($plan_period == '2' || $plan_period == 2){ 
								$months_period = $this->config->item('social_commerce_period');
								} 
								else if($plan_period == '3' || $plan_period == 3){ 
								$months_period = $this->config->item('sc_period_3_months_upgrad');
								} 
								else {
								$months_period = $this->config->item('social_commerce_period_2');
								}
								
								if($payment_status == 'Completed'){
									$data_timeperiod['expiry_date'] = $this->usersmodel->getexpiry_date($months_period); 
									$data_timeperiod['status'] = '1'; 
								}
								else{
									 //$data_timeperiod['status'] = '0';
									}
								//$data_timeperiod['expiry_date'] = $this->usersmodel->getexpiry_date($months_period); 
								//$data_timeperiod['subcription_date'] = date('Y-m-d');
								//$data_timeperiod['plan_id'] = 'social_commerce';	
								$this->db->set($data_timeperiod);
								$this->db->where('id',$waddressId);
								$this->db->update('waddresses');
								//increment counter in every call
								$this->db->set('ipn_count', 'ipn_count + 1', FALSE);
								$this->db->where('waddress_id',$waddressId);
								$this->db->update('payments');

								/* For manage Email templates for plan period*/
								if($payment_status == 'Completed'){
									$this->db->set('subscription_period', 'subscription_period + "'.$plan_period.'"', FALSE);
							        $this->db->where('id',$waddressId);
							        $this->db->update('waddresses');
        
								}

								@$getWaddressInfo = $this->commonmodel->get_item_by_id('waddresses',$waddressId);	
								
								$data['new_text'] =  '';
								$data['subscription_id'] =  $txn_id;
								$data['customerName'] =  $customerName;
								$data['waddress_name'] =  $getWaddressInfo->w_address;
								$data['plan_desc'] = 'Congratulations, your Social Commerce account extension is activated ';
								
								$this->load->library('email');
								$message = $this->load->view('mail/social_payment_renewal',$data,TRUE); 
								$this->email->initialize(array('mailtype' => 'html'));
								$this->email->from($this->config->item('base_email'), 'Social Commerce Account Extenction');
								$this->email->to($_player_email2); 
								$this->email->subject("W-Address Social Commerce Renewal Payment");             
								$this->email->message($message);
								$this->email->send();
		
		
							} else {
								
								////$data['waddress_id'] = $waddressId;
								$data['user_id'] = $uid;
								$data['waddress_id'] = $waddressId;
								$this->db->set($data);
								$this->db->insert('payments');
								/*Update User Table*/
								if($plan_period == '1' || $plan_period == 1){ 
								$months_period = $this->config->item('social_commerce_period_4');
								} 
								else if($plan_period == '2' || $plan_period == 2){ 
								$months_period = $this->config->item('social_commerce_period');
								} 
								else if($plan_period == '3' || $plan_period == 3){ 
								$months_period = $this->config->item('sc_period_3_months_upgrad');
								} 
								else {
								$months_period = $this->config->item('social_commerce_period_2');
								}
								//mail('bhavesh.khanpara@indianic.com', 'months_period', print_r($months_period,TRUE));	
								if($payment_status == 'Completed'){
									$data_timeperiod['expiry_date'] = $this->usersmodel->getexpiry_date($months_period); 
									$data_timeperiod['status'] = '1'; 
								}
								
								//$data_timeperiod['expiry_date'] = $this->usersmodel->getexpiry_date($months_period); 
								
								$data_timeperiod['subcription_date'] = date('Y-m-d');
								//$data_timeperiod['plan_id'] = 'social_commerce';
								//mail('bhavesh.khanpara@indianic.com', 'data_timeperiod', print_r($data_timeperiod,TRUE));	
								$this->db->set($data_timeperiod);
								$this->db->where('id',$waddressId);
								$this->db->update('waddresses');
								
								//increment counter in every call
								$this->db->set('ipn_count', 'ipn_count + 1', FALSE);
								$this->db->where('waddress_id',$waddressId);
								$this->db->update('payments');

								if($payment_status == 'Completed'){
									$this->db->set('subscription_period', 'subscription_period + "'.$plan_period.'"', FALSE);
							        $this->db->where('id',$waddressId);
							        $this->db->update('waddresses');
        
								}
		
								@$getWaddressInfo = $this->commonmodel->get_item_by_id('waddresses',$waddressId);	
								$data['new_text'] =  '';
								$data['subscription_id'] =  $txn_id;
								$data['customerName']    =  $customerName;
								$data['waddress_name']   =  $getWaddressInfo->w_address;
								$data['plan_desc'] = 'Congratulations, your Social Commerce account extension is activated ';
								
								$this->load->library('email');
								$message = $this->load->view('mail/social_payment_renewal',$data,TRUE); 
								$this->email->initialize(array('mailtype' => 'html'));
								$this->email->from($this->config->item('base_email'), 'Social Commerce Account Extenction');
								$this->email->to($_player_email2); 
								$this->email->subject("W-Address Social Commerce Renewal Payment");             
								$this->email->message($message);
								$this->email->send();
								
							}
							
					
					}
						else if($planId == '3' || $planId == 3)
						{	
							$data = array(
									'plan_id'           =>  $planId,
									'transaction_token' =>  $txn_id,
									'price'             =>  $payment_amount,
									'payment_status'    =>  $payment_status,
									'paypal_email'      =>  $_player_email,
									'created' 			=>  date('Y-m-d')
								);
								if($this->usersmodel->Payment_done_before_waddress($waddressId) == TRUE)
								{	
									/*Update Payments Table*/
									//$data['waddress_id'] = $waddressId;
									$this->db->set($data);
									$this->db->where('waddress_id',$waddressId);
									$this->db->update('payments');
									
									/*Update User Table*/
									$months_period = $this->config->item('commercial_period');
									//$data_timeperiod['expiry_date'] = $this->usersmodel->getexpiry_date($months_period); 
									if($payment_status == 'Completed'){
										$data_timeperiod['expiry_date'] = $this->usersmodel->getexpiry_date($months_period); 
										$data_timeperiod['status'] = '1'; 
									}
									
									$data_timeperiod['subcription_date'] = date('Y-m-d');
									//$data_timeperiod['plan_id'] = 'commercial';	
		
									$this->db->set($data_timeperiod);
									$this->db->where('id',$waddressId);
									$this->db->update('waddresses');
									
									@$getWaddressInfo = $this->commonmodel->get_item_by_id('waddresses',$waddressId);

									//increment counter in every call
									$this->db->set('ipn_count', 'ipn_count + 1', FALSE);
									$this->db->where('waddress_id',$waddressId);
									$this->db->update('payments');
									
									$data['new_text'] =  '';
									$data['subscription_id'] =  $txn_id;
									$data['customerName'] =  $customerName;
									$data['waddress_name'] =  $getWaddressInfo->w_address;
									$data['plan_desc'] = 'Congratulations, your annual Commercial W-Address subscription is active';

									$this->load->library('email');
									$message = $this->load->view('mail/commercial_payment_confirmation',$data,TRUE); 
									$this->email->initialize(array('mailtype' => 'html'));
									$this->email->from($this->config->item('base_email'), 'Commercial Subscription');
									$this->email->to($_player_email2); 
									$this->email->subject("W-Address Commercial Payment Confimation");             
									$this->email->message($message);
									$this->email->send();
							 
								} else {  
									/*Update Payments Table*/
									
									$data['user_id'] = $uid;
									$data['waddress_id'] = $waddressId;
									
									//mail('bhavesh.khanpara@indianic.com', 'data', print_r($data,TRUE));	
									$this->db->set($data);
									$this->db->insert('payments');
									//$this->commonmodel->insert('payments',$data);
									//mail('bhavesh.khanpara@indianic.com', 'qry', print_r($this->db->_error_message(),TRUE));	
									//mail('bhavesh.khanpara@indianic.com', 'qry', print_r($this->db->last_query(),TRUE));	
									//exit;
									$months_period = $this->config->item('commercial_period');
									if($payment_status == 'Completed'){
										$data_timeperiod['expiry_date'] = $this->usersmodel->getexpiry_date($months_period); 
										$data_timeperiod['status'] = '1'; 
									}
									//$data_timeperiod['expiry_date'] = $this->usersmodel->getexpiry_date($months_period); 
									$data_timeperiod['subcription_date'] = date('Y-m-d');
									//$data_timeperiod['plan_id'] = 'commercial';
			
									/*Update User Table*/
									$this->db->set($data_timeperiod);
									$this->db->where('id',$waddressId);
									$this->db->update('waddresses');
			
									//increment counter in every call
									$this->db->set('ipn_count', 'ipn_count + 1', FALSE);
									$this->db->where('waddress_id',$waddressId);
									$this->db->update('payments');
									
									@$getWaddressInfo = $this->commonmodel->get_item_by_id('waddresses',$waddressId);
									$data['new_text'] =  '';
									$data['subscription_id'] =  $txn_id;
									$data['customerName'] =  $customerName;
									$data['waddress_name'] =  $getWaddressInfo->w_address;
									//$data['plan_desc'] = 'This is to notify that you are sucessfully subscribed to commercial subscription plan.';
									$data['plan_desc'] = 'Congratulations, your annual Commercial W-Address subscription is active';
									
									$this->load->library('email');
									$message = $this->load->view('mail/commercial_payment_confirmation',$data,TRUE); 
									$this->email->initialize(array('mailtype' => 'html'));
									$this->email->from($this->config->item('base_email'), 'Commercial Subscription');
									$this->email->to($_player_email2); 
									$this->email->subject("W-Address Commercial Payment Confimation");             
									$this->email->message($message);
									$this->email->send();
							   }
							}			
				    	}
						
					else if($_REQUEST['txn_type'] == 'subscr_cancel')
					{
						
							
						/*	$userdata['status'] = '0';
							$userdata['w_address'] = '';
							$this->db->set($userdata);
							$this->db->where('user_id',$uid);
							$this->db->update('waddresses'); 
						*/
							$userdata = array();
							/*Remove form Database*/
							$this->db->where('user_id',$uid);
							$this->db->delete('waddresses');

							$this->db->where('id',$uid);
							$this->db->delete('users');

							$this->db->where('user_id',$uid);
							$this->db->delete('socials'); 

							$this->db->where('user_id',$uid);
							$this->db->delete('payments'); 
							
							$this->db->where('user_id',$uid);
							$this->db->delete('deals'); 

							$this->load->library('email');
							$data['name'] = $customerName;
							$data['desc'] = 'You are Sucessfully unsubscribe from our System.....';
							
							$message = $this->load->view('mail/accountexpiry',$data,TRUE); 
							$this->email->initialize(array('mailtype' => 'html'));
							$this->email->to($_player_email2);
							$this->email->from($this->config->item('email_from'), 'Waddress Team');
							$this->email->subject('Unsubscribe Plan Notification');                
							$this->email->message($message);
							$this->email->send();
							
							
							$data_array['name'] = 'Dear Admin';
							$data_array['desc'] = '<b>'.$customerName.'</b> Sucessfully unsubscribe from our System.....';
							$message = $this->load->view('mail/accountexpiry',$data_array,TRUE); 
							$this->email->initialize(array('mailtype' => 'html'));
							$this->email->to($this->config->item('email_from'));
							$this->email->from($this->config->item('email_from'), 'Waddress Team Notification');
							$this->email->subject('Unsubscribe Plan Notification');                
							$this->email->message($message);
							$this->email->send();
					
					}
					
			} 
			else if (strcmp ($res, "INVALID") == 0) 
			{
				$this->email->from($this->config->item('admin_email'), 'PAYMENT FAIL');
				$this->email->to($this->config->item('mail_from')); 
				$this->email->subject('PAYMENT FAIL');
				$this->email->message('Fail Processing.');	
				$this->email->send();
			}
		}
	}
}

