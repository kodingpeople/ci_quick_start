<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sendmail extends CI_Controller {
 public function __construct(){
  parent::__construct();
      $this->template->set('controller', $this);
      $this->load->model('commonmodel');
      $this->load->model('usersmodel');
      $this->load->helper(array('form', 'url'));
      $this->load->library('email');
 }

/*Index Method for Config settigns*/
public function index(){
  $this->load->helper('captcha');
  $vals = array(
     'word'          => random_string('numeric', 6),
     'img_path' 	 => './captcha/',
     'img_url' 		 => base_url().'captcha/',
     'img_width' 	 => '150',
     'img_height' 	 => 32,
     'border' 		 => 0,
     'expiration' 	 => 7200,
     );
  $cap 		= create_captcha($vals);
  $data 	= array(
     'captcha_time'  => $cap['time'],
     'ip_address'    => $this->input->ip_address(),
     'word'          => $cap['word']
     );
  $this->session->set_userdata($data);
  $data['cap_img']	=	$cap['image'];
  $this->template->load_partial('template_master', 'form_view', $data);
}

/*Index Method for Send Message to users */
public function add_message(){
    if($this->input->post('tokenemail') != ''){
        $this->session->set_userdata('sendmessage_emailFrom',@$this->input->post('tokenemail'));
    }

    $this->load->library('form_validation');
    $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]');
    $this->form_validation->set_rules('from_email', 'Your email', 'trim|required|valid_email');
    $this->form_validation->set_message('valid_email', 'Email field must contain a valid email address.');
    $this->form_validation->set_rules('subject', 'subject', 'trim|required');
    $this->form_validation->set_rules('message', 'Message', 'trim|required|xss_clean|min_length[10]|max_length[500]');
    $this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_check_captcha');
    $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
    
    if ($this->form_validation->run() == FALSE){
        // Repoplulate data again stuff here
        $this->index();
    } else {
        //send message stuff here
        $this->load->library('email');
        $data['name'] 	 = $this->input->post('name');
        $data['subject'] = $this->input->post('subject');
        $data['message'] = $this->input->post('message');
        $message 		 = $this->load->view('mail/sendmessage',$data,TRUE); 
        
		$this->email->initialize(array('mailtype' => 'html'));
        $this->email->to(base64_decode($this->session->userdata('sendmessage_emailFrom')));
        $this->email->from($this->input->post('from_email'), 'W-Address Team');
        $this->email->subject($this->input->post('subject'));                
        $this->email->message($message);
        $this->email->send();
        //$this->session->set_flashdata('search_error',$this->lang->line(''));
        $this->session->set_flashdata('info', 'Your Message sucessfully sent..Thank you!');
        redirect(base_url("sendmail?sucess=yes&token=".$this->session->userdata('sendmessage_emailFrom')));
        }
}

 public function check_captcha(){
    $expiration = 	time()-7200; // Two hour limit
    $cap		=	$this->input->post('captcha');
    
    if($this->session->userdata('word')						== 	$cap 
			AND $this->session->userdata('ip_address')		== 	$this->input->ip_address()
			AND $this->session->userdata('captcha_time')	> 	$expiration){
      return true;
    } else {
      $this->form_validation->set_message('check_captcha', 'Security number does not match.');
      return false;
    }

   }
}

?>