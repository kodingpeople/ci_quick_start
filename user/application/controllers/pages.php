<?php
class Pages extends CI_Controller
{
	private $data = array();
	private $table = 'users';
	
	function __construct() {
		parent::__construct();
		
		$this->template->set('controller', $this);
			$this->load->model('commonmodel');
			$this->load->model('usersmodel');
			$this->load->helper('form');
			$this->load->helper(array('form', 'url'));
			
	}
function index(){

		 $this->template->load_partial('template_master', 'pages', $this->data);


}

function cms($id){

	$this->data['page'] = $this->commonmodel->get_page_Id('cms',$id);
	/*echo '<pre>';
	print_r($this->data);*/
	$this->template->load_partial('template_master', 'pages', $this->data);
}
}	