<?php //if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mailcron extends CI_Controller {

	function __construct() {
		parent::__construct();
			$this->load->library('encrypt');
			$this->load->model('commonmodel');
			$this->load->model('usersmodel');
			$this->load->library('email');
            
	}

	function index()
	{    //$today = date('Y-m-d');
              //echo  $yesterday =  date('Y-m-d', strtotime($today. ' + 5 days'));exit;
        $userinfos = $this->commonmodel->getActiveUsers('users');
		foreach ($userinfos as $userinfo)
		{
            $getAllWaddressbyUserID  = $this->commonmodel->getWaddress_by_userId($userinfo->id);
            foreach($getAllWaddressbyUserID as $each)
            {
                $today = date('Y-m-d');
    			$yesterday =  date('Y-m-d', strtotime($today. ' - 1 days'));
    			$Date = $each->expiry_date;
                $sDate = $each->subcription_date;
    			$before_five_days_expiry  =  date('Y-m-d', strtotime($Date. ' - 5 days'));
    			$before_ten_days_expiry   =  date('Y-m-d', strtotime($Date. ' - 10 days'));
    			$before_fifteenDay_ago    =  date('Y-m-d', strtotime($Date. ' - 15 days'));
    			$before_thirtyDay_ago     =  date('Y-m-d', strtotime($Date. ' - 30 days'));
                $last_month_of_activation     =  date('m', strtotime($sDate. ' + 150 days'));
                $current_month     =  date('m');
                
                
                /*
                -> Delete User stuff here
                 Set user status inactivet if it expiry date is = yesterday date*/
                if((strtotime($yesterday) == strtotime($Date) ||  strtotime($yesterday) > strtotime($Date) || strtotime($today) == strtotime($each->expiry_date)) && $userinfo->role_id != '1')
                {   
                    $getTotal_Waddress_count = $this->commonmodel->waddress_count($userinfo->id);
                    if($getTotal_Waddress_count == '1'){
                        //Delete user and its all data
                        $this->db->where('waddress_id',$each->id);
                        $this->db->delete('socials');
                        
                        $this->db->where('waddress_id',$each->id);
                        $this->db->delete('payments'); 
                        
                        $this->db->where('waddress_id',$each->id);
                        $this->db->delete('deals');
                        
                        $this->db->where('id',$each->id);
                        $this->db->delete('waddresses');
                        
                        $this->db->where('id',$userinfo->id);
                        $this->db->delete('users');
                        
                        $data['name'] = ucfirst($userinfo->first_name.' '.$userinfo->last_name);
                        $data['desc'] = 'Your account has been closed and your W-<b>'.$each->w_address.'</b> is now open and avalible to public for activation.We hope our service was of value and you\'ll use it again in the near future';
                        $message = $this->load->view('mail/accountexpiry',$data,TRUE); 
                        $this->email->initialize(array('mailtype' => 'html'));
                        $this->email->to($userinfo->email);
                        $this->email->from($this->config->item('email_from'), 'Waddress Team');
                        $this->email->subject('W-Address Account Deletion Confirmation');                
                        $this->email->message($message);
                        $this->email->send();
                    }else{
                        
                        $this->db->where('waddress_id',$each->id);
                        $this->db->delete('socials');
                        
                        $this->db->where('waddress_id',$each->id);
                        $this->db->delete('payments'); 
                        
                        $this->db->where('waddress_id',$each->id);
                        $this->db->delete('deals');
                        
                        $this->db->where('id',$each->id);
                        $this->db->delete('waddresses');
                        
                        //Delete individual W-address
                        $data['name'] = ucfirst($userinfo->first_name.' '.$userinfo->last_name);
                        $data['desc'] = 'Your account has been closed and your W-<b>'.$each->w_address.'</b> is now open and avalible to public for activation.We hope our service was of value and you\'ll use it again in the near future';
                        $message = $this->load->view('mail/accountexpiry',$data,TRUE); 
                        $this->email->initialize(array('mailtype' => 'html'));
                        $this->email->to($userinfo->email);
                        $this->email->from($this->config->item('email_from'), 'Waddress Team');
                        $this->email->subject('W-Address Account Deletion Confirmation');               
                        $this->email->message($message);
                        $this->email->send();
                        
                    }
                }

                /* Send a mail before 10 days ago of expiration
                - Mail will send after 350 days for personal user
                */	
    			if($userinfo->plan_id == 'personal' && $userinfo->role_id == '2')
                {
    				if(strtotime($today) == strtotime($before_fifteenDay_ago))
                    {       
                        //$data['name'] = $userinfo->first_name.' '.$userinfo->last_name;
                        $data['name'] = $userinfo->last_name;
                        $data['plan'] = $userinfo->plan_id;
                        //'.$userinfo->plan_id.'
                        $link =  site_url('users/userActivation/?key='.base64_encode($userinfo->last_name).'&wid='.$each->id); 
                        $data['desc'] = 'This is your annual account activity verification.</br>Please respond within 15 days or your account will be systematically closed and your unique W-Address ID will become open to the public for activation.
                        <br/>please  <a href="'.$link.'">   click here to confirm activity </a>';

                        $message = $this->load->view('mail/personal_account_verification',$data,TRUE); 
                        $this->email->initialize(array('mailtype' => 'html'));
                        $this->email->to($userinfo->email);
                        $this->email->from($this->config->item('email_from'), 'W-Address Team');
                        $this->email->subject('W-address Personal account activity verification');                
                        $this->email->message($message);
                        $this->email->send();
                    }
    			}
    			elseif($userinfo->plan_id == 'commercial' && $userinfo->role_id == '2')
    			{
                    /* Send a mail before 15 days ago of expiration
                    - This is for Commercial user 
                    - if Payment type is m
                    */	
		            if(strtotime($today) == strtotime($before_fifteenDay_ago) && $userinfo->payment_type == 'm')
        			{
        				$this->load->library('email');
						//$data['name'] = $userinfo->first_name.' '.$userinfo->last_name;
						$data['name'] = $userinfo->last_name;
						$data['plan'] = $userinfo->plan_id;
						//'.$userinfo->plan_id.'
						$data['desc'] = 'Your Commercial subcription will expire in 15 days and there is NOT a grace period.Once it expires it becomes open and avalible for public activation.<br/>You\'re encouraged to renew your W-address sooner then later.<br/>
                        <a href="'.base_url('users/planCupgrade/?key='.base64_encode($userinfo->last_name).'&wid='.$each->id.'&type=Upgrade').'">click here</a> and you\'ll be directed to our Paypal link to complete the simple process
                        ';
						$data['paypal_link'] = base_url('users/planCupgrade/?key='.base64_encode($userinfo->last_name).'&wid='.$each->id); 
						$message = $this->load->view('mail/commercial_account_manual_renewal',$data,TRUE); 
						$this->email->initialize(array('mailtype' => 'html'));
						$this->email->to($userinfo->email);
						$this->email->from($this->config->item('email_from'), 'W-Address Team');
						$this->email->subject('W-Address Commercial Account Renewal Notification');                
						$this->email->message($message);
						$this->email->send();
        			}
					
                    /* Send a mail before 15 days ago of expiration
                    - This is for Commercial user  if payment type is auto recurring
                    */	
			        elseif(strtotime($today) == strtotime($before_fifteenDay_ago) && $userinfo->payment_type == 'a')
			        {
						//$data['name'] = $userinfo->first_name.' '.$userinfo->last_name;
						$data['name'] = $userinfo->last_name;
						$data['plan'] = $userinfo->plan_id;
                        $data['paypal_link'] = base_url('users/planCupgrade/?key='.base64_encode($userinfo->last_name).'&wid='.$each->id); 
						$data['desc'] = 'Your Commercial subcription will expire in 15 days and there is NOT a grace period.Once it expires it becomes open and avalible for public activation.<br/>You\'re encouraged to renew your W-address sooner then later.<br/>
                        <a href="'.base_url('users/planCupgrade/?key='.base64_encode($userinfo->last_name).'&wid='.$each->id.'&type=Upgrade').'">click here</a> and you\'ll be directed to our Paypal link to complete the simple process
                        ';
						//$data['desc'] = 'Your Subcription '.$userinfo->plan_id.' plan will be Auto Subscribe in next 30 Days';
						
						$message = $this->load->view('mail/commercial_account_manual_renewal',$data,TRUE); 
						$this->email->initialize(array('mailtype' => 'html'));
						$this->email->to($userinfo->email);
						$this->email->from($this->config->item('email_from'), 'Waddress Team');
						$this->email->subject('Commercial Plan Auto Upgrade Notification ');                
						$this->email->message($message);
						$this->email->send();
                    }
    			}
			
            elseif($userinfo->plan_id == 'social_commerce') 
            {	
               	//before 5 days ago and check it is last month then only send no extenction will required
                if($last_month_of_activation == $current_month &&  $each->subscription_period == '6' && strtotime($today) == strtotime($before_five_days_expiry))
                {
                    //Check if it is do subscription for 4 moths then send mail without paypal link
                    //else send him to pay 1 months subscription link
                    $data['name'] = $userinfo->last_name;
                    $data['plan'] = $userinfo->plan_id;
                    $data['desc'] = 'Your social Commerce account expires in 5 days and is NO LONGER eligle for renewal.Once expired it becomes open to the
                    public, however if you find it to be of permanent value then you can purchase it as a Commercial W-Address for $39.00/yr
                    ';
                    
                    $message = $this->load->view('mail/before_onemonth_sc',$data,TRUE); 
                    $this->email->initialize(array('mailtype' => 'html'));
                    $this->email->to($userinfo->email);
                    $this->email->from($this->config->item('email_from'), 'W-Address Team');
                    $this->email->subject('W-Address Social Commerce Expiration Notice');                
                    $this->email->message($message);
                    $this->email->send(); 
                } else if($each->subscription_period <= '5' && strtotime($today) == strtotime($before_five_days_expiry)) {
		        //send mail before before 5 days

                    $MaxLimit = 6;
                    $pending_months  = $MaxLimit - $each->subscription_period;

                    $data['name'] = $userinfo->last_name;
                    $data['plan'] = $userinfo->plan_id;
                    
                    $paypal_link_n = base_url('planupgrading/?key='.base64_encode($userinfo->last_name).'&wid='.$each->id.'&months='.$pending_months.'&type=Upgrade'); 
                    $paypal_link_1 = base_url('planupgrading/?key='.base64_encode($userinfo->last_name).'&wid='.$each->id.'&months=1&type=Upgrade'); 
                        
                    if($pending_months == '4'){

                        $data['desc'] = 'Your FREE Social Commerce subscription expires in 5 days and there\'s NO grace period.Terms are listed below if you need an extension';
                        $data['table'] =
                                '<table width="521" height="74" border="0" align="center" cellpadding="3" cellspacing="3" style="font-family:calibri, Helvetica, sans-serif; font-size:16px;; line-height:18px; padding:0 25px;">
                                <tr>
                                <td><strong>Options available</strong></td>
                                <td>&nbsp;</td>
                                </tr>
                                <tr>
                                <td>$9.95 one month at a time</td>
                                <td><a href="'.$paypal_link_1.'">Upgrade</a></td>
                                </tr>
                                <tr>
                                <td>$39.80 for 4 months (Non-refundable)</td>
                                <td><a href="'.$paypal_link_n.'">Upgrade</a></td>
                                </tr>
                                </table>';
                    }

                    if($pending_months == '3'){
                        $data['desc'] = 'Your first 30 days extension expires in 5 days and there\'s NO grace period.Terms are listed below if you need additional time';
                        $data['table'] =
                                '<table width="521" height="74" border="0" align="center" cellpadding="3" cellspacing="3" style="font-family:calibri, Helvetica, sans-serif; font-size:16px;; line-height:18px; padding:0 25px;">
                                <tr>
                                <td><strong>Options available</strong></td>
                                <td>&nbsp;</td>
                                </tr>
                                <tr>
                                <td>$9.95 one month at a time</td>
                                <td><a href="'.$paypal_link_1.'">Upgrade</a></td>
                                </tr>
                                <tr>
                                <td>$29.85 for 3 months (Non-refundable)</td>
                                <td><a href="'.$paypal_link_n.'">Upgrade</a></td>
                                </tr>
                                </table>';
                        
                    }

                    if($pending_months == '2'){
                        $data['desc'] = 'Your second 30 day extension expires in 5 days and there\'s NO grace period.Terms are listed below if you need additional time';
                        $data['table'] =
                                '<table width="521" height="74" border="0" align="center" cellpadding="3" cellspacing="3" style="font-family:calibri, Helvetica, sans-serif; font-size:16px;; line-height:18px; padding:0 25px;">
                                <tr>
                                <td><strong>Options available</strong></td>
                                <td>&nbsp;</td>
                                </tr>
                                <tr>
                                <td>$9.95 one month at a time</td>
                                <td><a href="'.$paypal_link_1.'">Upgrade</a></td>
                                </tr>
                                <tr>
                                <td>$19.90 for 2 months (Non-refundable)</td>
                                <td><a href="'.$paypal_link_n.'">Upgrade</a></td>
                                </tr>
                                </table>';
                    }     

                    if($pending_months == '1'){
                        $data['desc'] = 'Your Final extension expires in 5 days and there\'s NO grace period.Terms are listed below if you need additional time';
                        $data['table'] =
                                '<table width="521" height="74" border="0" align="center" cellpadding="3" cellspacing="3" style="font-family:calibri, Helvetica, sans-serif; font-size:16px;; line-height:18px; padding:0 25px;">
                                <tr>
                                <td><strong>Options available</strong></td>
                                <td>&nbsp;</td>
                                </tr>
                                <td>$9.95 for 1 months (Non-refundable)</td>
                                <td><a href="'.$paypal_link_1.'">Upgrade</a></td>
                                </tr>
                                </table>'; 
                    }                    
                        
                    $message = $this->load->view('mail/common_template_for_sc_plan_upgrade',$data,TRUE); 
                    $this->email->initialize(array('mailtype' => 'html'));
                    $this->email->to($userinfo->email);
                    $this->email->from($this->config->item('email_from'), 'W-Address Team');
                    $this->email->subject('W-Address Social Commerce Renewal Notification');                
                    $this->email->message($message);
                    $this->email->send();
                    //echo $this->email->print_debugger();
               }
            }
		  }
       }
	}
}