<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {

		parent::__construct();
		
		$this->template->set('controller', $this);
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('encrypt');
		if($this->nic->logged_in() == TRUE){

			redirect('paymentview');
		}
	}
	
	

	/* Method for load Landing or Homepage*/
	public function index(){
		//echo $plans_price;
		
		// Plan Price Array
		$plans_price = array(
							'personal' => $this->config->item('personal_plan'),
							'plan_social_commerce' => $this->config->item('social_commerce_plan'),
							'plan_commercial' => $this->config->item('commercial_plan')
							);

		$this->data['plans_price'] = $plans_price;
		$this->template->load_partial('template_master', 'home', $this->data);
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */