-- phpMyAdmin SQL Dump
-- version 2.11.8.1deb5+lenny3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 13, 2014 at 12:53 PM
-- Server version: 5.0.51
-- PHP Version: 5.2.4-2ubuntu5.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mwaddress`
--

-- --------------------------------------------------------

--
-- Table structure for table `business_details`
--

CREATE TABLE IF NOT EXISTS `business_details` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `waddress_id` int(11) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `business_pic` varchar(255) NOT NULL,
  `brand_logo` varchar(255) NOT NULL,
  `address_1` varchar(255) NOT NULL,
  `area_code` int(11) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `phone_publish` int(11) NOT NULL default '1',
  `city` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `country` varchar(200) NOT NULL,
  `zipcode` int(11) NOT NULL,
  `web_address` varchar(150) NOT NULL,
  `email` varchar(200) NOT NULL,
  `intro_text` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `waddress_id` (`waddress_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=79 ;

--
-- Dumping data for table `business_details`
--

INSERT INTO `business_details` (`id`, `user_id`, `waddress_id`, `company_name`, `business_pic`, `brand_logo`, `address_1`, `area_code`, `phone`, `phone_publish`, `city`, `state`, `country`, `zipcode`, `web_address`, `email`, `intro_text`, `created`, `modified`) VALUES
(1, 101, 2, 'indianic infotech ltd', '', '', '2502,yahoo stoe', 0, '23423423423424', 1, '', '', '', 0, 'http://www.indianic.com', 'info@indianic.com', 'Best IT Solution Provider', '2013-12-19 11:33:31', '2013-12-23 15:17:33'),
(2, 100, 1, 'IndiaNIC infotech', '', '1387446056.png', 'indianic ltd,iskon cross road,ahmedabad,gujarat', 0, '079(99846523)', 0, '', '', '', 0, 'http://www.indianic.com', 'Email@indianic.com', 'Type here your business Description', '2013-12-19 11:39:42', '2013-12-19 15:35:08'),
(3, 100, 4, 'IndiaNIC infotech', '', '1387447801.png', 'indianic ltd,iskon cross road,ahmedabad,gujarat', 0, '079(99846523)', 1, '', '', '', 0, 'http://www.indianic.com', 'pradeep.khot@indianic.com', 'Type here your business Description', '2013-12-19 08:10:35', '2013-12-19 15:41:02'),
(6, 108, 43, 'indianic infotech ltd', '', '', '201, 2nd Floor, Dev Arc Mall, Satellite Rd,  INDIA', 0, '13131313131', 0, '', '', '', 0, 'http://www.indianic.com', 'info@indianic.com', 'Complete IT Solution Provider!!!!', '2013-12-20 15:56:44', '2013-12-20 16:02:10'),
(7, 109, 45, 'indianic infotech ltd', '', '', '201, 2nd Floor, Dev Arc Mall, Satellite Rd, Off S.G Highway, Near Iskcon Bridge, Ahmedabad, Gujarat, 380015. INDIA', 0, '13131313123', 0, '', '', '', 0, 'http://www.indianic.com', 'zalak.patel@indianic.com', 'Best IT Solution Provider', '2013-12-20 16:24:14', '2013-12-20 16:24:14'),
(11, 119, 54, 'IndiaNIC infotech', '', '1387885157.png', 'indianic ltd,iskon cross road,ahmedabad,gujarat', 0, '(555) 555-5555', 1, '', '', '', 0, 'http://www.indianic.com', 'bhavesh.khanpara@indianic.com', 'Type here your business Description', '2013-12-24 17:07:18', '2013-12-24 17:09:18'),
(12, 124, 58, '', '', '', '', 0, '', 0, '', '', '', 0, '', '', '', '2013-12-27 14:36:55', '2013-12-27 14:36:55'),
(13, 117, 52, 'IndiaNIC infotech898989898', '', '1388826251.png', 'indianic', 0, '(555) 555-5555', 1, '', '', '', 0, '0', 'bhavesh.khanpara@indianic.com', 'I am a php developer with 5+ years commercial experience in PHP/MySQL – (LAMP). I have very good commercial experience in analysis, design, development, testing and implementation of web applications. Selfmade and good communicator which help me to work with clients at all levels of the business. I am very strict in meeting deadlines which is a critical aspect for any business. All my current work is developed using the latest version of PHP and MySQL and my code is upto the current standards ..Sample test Content here', '2014-01-04 06:17:31', '2014-01-04 13:19:29'),
(14, 118, 53, 'IndiaNIC infotech Complete IT solution', '', '1388812849.png', 'indianic ltd,iskon cross road,ahmedabad,gujarat', 0, '(555) 555-5555', 1, '', '', '', 0, '0', 'bhavesh.khanpara@indianic.com', 'Type here your business Description 5656565', '2014-01-04 06:25:41', '2014-01-04 14:26:34'),
(15, 125, 59, '', '', '', '', 0, '', 0, '', '', '', 0, '', '', '', '2014-01-04 07:31:30', '2014-01-04 07:31:30'),
(16, 123, 57, 'IndiaNIC infotech', '', '1388981719.png', '', 0, '(555) 555-5555', 0, '', '', '', 0, '0', '', 'Here Type your Business Description', '2014-01-06 05:15:36', '2014-01-06 05:15:36'),
(17, 126, 60, 'beautiful animal', '', '1390343457.jpg', '', 0, '', 1, '', '', '', 0, '0', '', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '0000-00-00 00:00:00', '2014-01-25 05:16:30'),
(18, 127, 61, '', '', '', '', 0, '', 1, '', '', '', 0, '', '', '', '0000-00-00 00:00:00', '2014-01-06 13:13:08'),
(19, 131, 67, 'NewTabP', '', '', '', 0, '(555) 555-5555', 1, '', '', '', 0, '0', '', 'Type Here your Personal Description here ...\r\n\r\nThank you :)', '2014-01-06 14:21:35', '2014-01-20 12:40:25'),
(20, 132, 68, 'IndiaNIC infotech', '', 'd2fbeb36a674e3f6532f91225a31e48e.png', '', 0, '(555) 555-5555', 1, '', '', '', 0, '0', '', 'PHP Developement', '2014-01-06 17:10:56', '2014-01-07 12:48:50'),
(21, 133, 69, 'IndiaNIC', '', '1390463868.png', '', 0, '(555) 555-5555', 1, '', '', '', 0, 'http://www.indianic.com', '', 'dssdsadsa', '2014-01-06 12:33:53', '2014-01-23 08:57:49'),
(22, 0, 62, '', '', '', '', 0, '', 1, '', '', '', 0, '', '', 'dsadsad', '2014-01-06 11:00:28', '2014-01-06 15:32:54'),
(23, 134, 70, 'this is the copy of the w-address video script word for word', '', '1389126573.jpg', '', 0, '7145551212', 1, '', '', '', 0, '0', '', 'They’re called “smart phones”, but it’s frustrating scrolling search intensive web browsers and manipulating small screens only to discover the site you’re trying to view is not suited for a mobile device. Not anymore! Move over “Dot com’s” there’s a new sheriff in cyber town called W-Address, The Mobile Web Address, enabling everyone to better connect and become more interactive in the dynamic mobile environment!\nW-Address empowers subscribers with resources to design exclusive Mobile Web Addresses. Simply select a category, choose a unique name then tailor your site with contact info, a photo, relevant content and various Social Media connections. Publish and display your W-Address and get discovered by a device crazed culture!   \nUnlike the structured format of domain names the W symbol can vary in size and be sensibly placed anywhere next to or around a W-Address which then seamlessly connects to the selected page when typed into the onsite data field.\nPersonal ID’s are FREE and can be used on calling cards, resumes, personal stationary, social sites and in videos.\nSocial Commerce labels are FREE for 60 days and are productively resourceful when posted on public signs used on items or properties For Sale, Rent or Lease and to provide information and directions to social gatherings, Garage and Estate sales.\nCommercial names are $39.00/year and will cost effectively and informatively introduce consumers to your product, service or profession as well as stimulate Social Media activity. Display W-Addresses in storefronts and signs, in print and all varieties of visual media. Innovatively purchase several brand related names to showcase on billboards, banners, buses, bus stops, and blimps to effectively geo-target numerous markets simultaneously. Take advantage of “WOW Deals” the inclusive marketing tool designed to boost business by creating and publishing Social buying deals on your site.', '2014-01-08 02:00:04', '2014-01-08 22:33:46'),
(24, 135, 71, 'test007', '', '1389127129.jpg', '', 0, '', 0, '', '', '', 0, '0', '', 'jdklchdohiherohe', '2014-01-08 02:09:06', '2014-01-08 02:09:06'),
(25, 138, 73, 'Hallie', '', '1389201995.jpg', '', 0, '949-279-8305', 1, '', '', '', 0, '0', '', 'I have the best life ever!', '2014-01-08 22:57:05', '2014-01-14 22:32:26'),
(26, 141, 74, 'bargain babe rentals $10 all day', '', '1390608430.jpg', '', 0, '7143625868', 0, '', '', '', 0, 'http://www.bargainbikerentals.com/', '', 'They’re called “smart phones”, but it’s frustrating scrolling search intensive web browsers and manipulating small screens only to discover the site you’re trying to view is not suited for a mobile device. Not anymore! Move over “Dot com’s” there’s a new sheriff in cyber town called W-Address, The Mobile Web Address, enabling everyone to better connect and become more interactive in the dynamic mobile environment!\nW-Address empowers subscribers with resources to design exclusive Mobile Web Addresses. Simply select a category, choose a unique name then tailor your site with contact info, a photo, relevant content and various Social Media connections. Publish and display your W-Address and get discovered by a device crazed culture!', '2014-01-09 05:23:04', '2014-01-25 05:37:13'),
(27, 141, 75, 'Bargain babe rentals sunset beach', '', '1389230602.jpg', '', 0, '', 0, '', '', '', 0, 'http://www.babeschicken.com/', '', '', '2014-01-09 06:57:51', '2014-02-02 05:23:38'),
(28, 144, 77, 'Name to display on page', '', '', '', 0, '(555) 111-1234', 1, '', '', '', 0, '0', '', 'type info to display', '2014-01-13 20:51:44', '2014-01-21 05:00:56'),
(29, 145, 78, 'name on page', '', '1389664366.png', '', 0, '630 555 1111', 1, '', '', '', 0, '0', '', 'info page', '2014-01-14 07:23:04', '2014-01-14 07:23:04'),
(30, 146, 79, 'name on page', '', '1389664820.png', '', 0, '555 5555555', 0, '', '', '', 0, 'http://www.google.com', '', 'info on page', '2014-01-14 07:32:12', '2014-01-14 07:32:12'),
(31, 146, 80, 'name', '', '', '', 0, '', 1, '', '', '', 0, '', '', 'info number 2', '2014-01-14 07:34:04', '2014-01-14 07:34:19'),
(32, 148, 82, 'Hawlie', '1389735002.jpg', 'logo1389735802.jpg', '', 0, '949-279-8305', 1, '', '', '', 0, '0', '', 'I''m so spoiled', '2014-01-15 03:00:02', '2014-01-15 03:13:22'),
(33, 150, 83, 'id', '', 'logo1389747092.jpg', '', 0, '555 5555555', 0, '', '', '', 0, '0', '', 'Info to display', '2014-01-15 06:21:32', '2014-01-17 05:20:47'),
(34, 141, 85, 'Babes Brewing Company', '', '1389916792.JPG', '', 0, '', 0, '', '', '', 0, 'http://www.babesbrewing.com', '', 'Beautifully crafted beer!', '2014-01-17 05:31:17', '2014-01-17 05:42:23'),
(35, 133, 87, '', '', '', '', 0, '', 0, '', '', '', 0, '', '', '', '2014-01-17 10:17:38', '2014-01-17 16:45:32'),
(36, 152, 88, 'IndiaNIC infotech', '', '1390020633.jpg', '', 0, '(555) 555-5555', 1, '', '', '', 0, '0', '', 'Type your desc herer', '2014-01-18 10:20:50', '2014-01-18 10:20:50'),
(37, 159, 98, '', '', '', '', 0, '', 0, '', '', '', 0, '0', '', '', '2014-01-21 10:34:28', '2014-01-21 10:36:24'),
(38, 133, 99, '', '', '1390280984.png', '', 0, '', 0, '', '', '', 0, '', '', '', '2014-01-21 10:39:46', '2014-01-21 10:39:46'),
(39, 160, 100, '', '', '', '', 0, '', 0, '', '', '', 0, '0', '', '', '2014-01-21 10:44:18', '2014-01-21 10:44:17'),
(40, 133, 91, '', '', '', '', 0, '', 0, '', '', '', 0, '', '', '', '2014-01-21 07:17:23', '2014-01-21 07:17:47'),
(41, 158, 96, '', '', '', '', 0, '', 0, '', '', '', 0, '', '', '', '2014-01-21 20:42:33', '2014-01-21 20:42:33'),
(42, 156, 93, 'Buy your quad sak today', '', '1390351269.jpg', '', 0, '', 0, '', '', '', 0, '0', '', 'Quad sak is the all activity accessory it can be used to change clothes, as a backpack take a shower and a wet storage bag\nPrice at only $29', '2014-01-22 06:13:23', '2014-01-22 06:13:52'),
(43, 141, 95, 'Babes and brew', '', '1390610163.jpg', '', 0, '', 0, '', '', '', 0, 'http://www.babesbrewing.com', '', 'Come in for the tasty brew and check out the beautiful babes too', '2014-01-24 16:54:13', '2014-01-25 06:17:20'),
(44, 141, 76, '', '', '', '', 0, '7145551212', 0, '', '', '', 0, '', '', '', '2014-01-25 05:10:44', '2014-01-25 05:10:44'),
(45, 141, 102, 'Hooter Brew', '', '1390608775.JPG', '', 0, '', 0, '', '', '', 0, 'http://hooterbrew.com', '', 'best brew and babes too!', '2014-01-25 05:44:04', '2014-01-25 06:04:16'),
(46, 164, 104, 'eastern high sierras', '', '1391202496.JPG', '', 0, '+491764444305', 1, '', '', '', 0, 'http://www.tomsplaceresort.com/', '', 'a beautiful place to camp and fish', '2014-02-01 02:26:11', '2014-02-01 04:15:24'),
(47, 165, 105, 'dr evil', '', '1391202350.jpg', '', 0, '7143625768', 0, '', '', '', 0, '0', '', 'in honor of my dearly departed friend stephen mekeel', '2014-02-01 02:33:10', '2014-02-01 08:08:20'),
(48, 167, 116, 'flea', '', 'logo1391458686.jpg', '', 0, '', 0, '', '', '', 0, '0', '', 'My favorite dog', '2014-02-04 01:47:29', '2014-02-04 01:48:06'),
(49, 168, 117, 'Rolls Royce', '', 'logo1391459478.jpg', '', 0, '', 0, '', '', '', 0, '', '', 'Nice ride!', '2014-02-04 02:01:18', '2014-02-04 02:02:46'),
(50, 173, 142, 'IndiaNIC', '', '1391573431.png', '', 0, '', 0, '', '', '', 0, '0', '', 'Welcome to W-address', '2014-02-05 09:40:55', '2014-02-05 09:40:55'),
(51, 174, 143, 'IndiaNIC', '', '1391590381.png', '', 0, '', 1, '', '', '', 0, 'http://www.indianic.com', '', 'Welcome to W-Address.', '2014-02-05 14:23:23', '2014-02-05 14:23:42'),
(52, 175, 144, 'IndiaNIC', '', '1391591298.png', '', 0, '', 1, '', '', '', 0, 'http://www.indianic.com', '', 'type here your desc', '2014-02-05 14:38:37', '2014-02-05 14:38:37'),
(54, 177, 147, 'NewTabP', '', '1391596488.png', '', 0, '', 0, '', '', '', 0, 'http://www.indianic.com', '', 'fgdfg', '2014-02-05 16:05:12', '2014-02-05 16:06:33'),
(55, 178, 151, 'Rolls Royce', '1391647416.jpg', 'logo1391642089.jpg', '', 0, '+491764444305', 1, '', '', '', 0, '0', '', 'Fyyg opp o uhh gfffff', '2014-02-06 04:44:49', '2014-02-06 06:13:36'),
(63, 190, 161, 'IndiaNIC', '', '', '', 0, '', 0, '', '', '', 0, 'http://www.indianic.com', '', 'Type your description here', '2014-02-06 16:23:10', '2014-02-06 16:23:10'),
(64, 191, 165, 'Bark bark', '1391716263.jpg', 'logo1391716263.jpg', '', 0, '9495559999', 1, '', '', '', 0, '0', '', 'Bark bark from Hallie the spoiled dog', '2014-02-07 01:18:21', '2014-02-07 01:21:03'),
(65, 192, 166, 'happy salt and pepper', '', 'logo1391722968.jpg', '', 0, '+491764444305', 1, '', '', '', 0, '', '', 'Wow even my salt and pepper greet me with a smile!', '2014-02-07 03:07:59', '2014-02-07 03:16:45'),
(66, 194, 168, 'IndiaNIC', '', '1391748922.png', '', 0, '', 0, '', '', '', 0, 'http://www.indianic.com', '', 'Hi ,\nThis is test description here', '2014-02-07 10:25:45', '2014-02-11 10:22:10'),
(67, 194, 169, 'IndiaNIC', '', '', '', 0, '', 0, '', '', '', 0, 'http://www.indianic.com', '', 'type your description here', '2014-02-07 10:38:52', '2014-02-07 12:14:42'),
(68, 195, 170, '', '', '', '', 0, '', 0, '', '', '', 0, '0', '', '', '2014-02-10 10:57:44', '2014-02-10 10:57:44'),
(69, 196, 171, 'devid007 testing', '', '', '', 0, '', 0, '', '', '', 0, '0', '', 'devid007 is the best PHP developer...', '2014-02-10 17:04:15', '2014-02-10 17:04:15'),
(70, 197, 172, 'Setsession', '', '1392098073.jpg', '', 0, '(555) 555-5555', 1, '', '', '', 0, 'http://www.indianic.com', '', 'Hello Type your business details here', '2014-02-11 11:11:16', '2014-02-11 11:24:34'),
(72, 198, 174, 'IndiaNIC', '', '1392099462.jpg', '', 0, '(555) 555-5555', 1, '', '', '', 0, '0', '', 'hi this is test desc', '2014-02-11 11:48:00', '2014-02-11 11:48:00'),
(73, 199, 175, 'bhavesh_personal', '', '', '', 0, '(555) 555-5555', 1, '', '', '', 0, '0', '', 'bhavesh_personal', '2014-02-11 11:55:45', '2014-02-11 11:55:45'),
(74, 197, 179, '', '', '', '', 0, '', 0, '', '', '', 0, '', '', '', '2014-02-11 18:23:07', '2014-02-11 18:23:07'),
(75, 200, 180, 'Ryan Personal Page', '', '1392182185.jpg', '', 0, '(555) 555-5555', 1, '', '', '', 0, '0', '', 'Hello all, \nPlease type your Personal information here and make it public so other user can see and contact you .', '2014-02-12 10:48:04', '2014-02-12 10:48:04'),
(76, 201, 181, 'Ryan002 sc plan', '', '1392185943.png', '', 0, '(555) 555-5555', 1, '', '', '', 0, '0', '', 'I want to sell my Iphone 5 Please contact me ASAP. click on + icon to see large image. and click on Email or Phone icon to contact me.', '2014-02-12 11:49:11', '2014-02-12 11:50:27'),
(77, 202, 182, 'Ryan003_commercial', '', '1392187727.png', '', 0, '(555) 555-5555', 1, '', '', '', 0, 'http://www.indianic.com', '', 'Ryan003_commercial - Please contact for creating a new site or mobile app .', '2014-02-13 06:09:43', '2014-02-13 10:36:36'),
(78, 202, 183, 'IndiaNIC', '', '1392189323.png', '', 0, '(555) 555-5555', 1, '', '', '', 0, 'http://www.indianic.com', '', 'Type your description here', '2014-02-12 12:45:49', '2014-02-12 12:45:49');

-- --------------------------------------------------------

--
-- Table structure for table `cms`
--

CREATE TABLE IF NOT EXISTS `cms` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(200) default NULL,
  `text` text,
  `seo_page_title` varchar(200) default NULL,
  `slug` varchar(150) default NULL,
  `meta_keywords` varchar(150) default NULL,
  `meta_description` varchar(150) default NULL,
  `lang` varchar(3) default NULL,
  `status` tinyint(4) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `cms`
--

INSERT INTO `cms` (`id`, `title`, `text`, `seo_page_title`, `slug`, `meta_keywords`, `meta_description`, `lang`, `status`) VALUES
(11, 'Terms and Conditions', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>\n            <p>Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>', 'Terms of Use', 'About us', 'Terms of Use', 'Terms of Use', 'en', 1),
(13, 'Privacy Policy ', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>\r\n            <p>Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>', '', 'Privacy Policy ', '', '', 'en', 1),
(14, 'Terms and Condition', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>\r\n            <p>Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>', 'indianic', 'indianic', 'indianic', 'indianic', 'en', 1),
(15, 'contact us', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>\r\n            <p>Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>', 'contact us', 'contact us', 'contact us', 'contact us', 'en', 1),
(17, 'The New Sheriff of Cyber Town', '<div><p class="MsoNormal">They’re called “smart phones”, but it’s frustrating scrolling\nsearch intensive web browsers and manipulating small screens only to discover\nthe site you’re trying to view is not suited for a mobile device. Not anymore! Move\nover “Dot com’s” there’s a new sheriff in cyber town called W-Address, The\nMobile Web Address, enabling everyone to better connect and become more interactive\nin the dynamic mobile environment!</p>\n\n<p class="MsoNormal"><u>W</u>-Address empowers subscribers\nwith resources to design exclusive Mobile Web Addresses. Simply select a\ncategory, choose a unique name then tailor your site with contact info, a\nphoto, relevant content and various Social Media connections. Publish and display\nyour W-Address and get discovered by a device crazed culture! &nbsp;&nbsp;<o:p></o:p></p>\n\n<p class="MsoNormal">Unlike the structured\nformat of domain names the <b><u>W</u></b> symbol\ncan vary in size and be sensibly placed anywhere next to or around a W-Address which\nthen seamlessly connects to the selected page when typed into the onsite data\nfield. <o:p></o:p></p>\n\n<p class="MsoNormal">Personal<b> </b>ID’s are FREE and can be used on\ncalling cards, resumes, personal stationary, social sites and in videos. <o:p></o:p></p>\n\n<p class="MsoNormal">Social<b><i> </i></b>Commerce\nlabels are FREE for 60 days and are productively resourceful when posted on public\nsigns used on items or properties For Sale, Rent or Lease and to provide\ninformation and directions to social gatherings, Garage and Estate sales.<o:p></o:p></p>\n\n<p class="MsoNormal">Commercial<b> </b>names are $39.00/year and will cost\neffectively and informatively introduce consumers to your product, service or\nprofession as well as stimulate Social Media activity. Display W-Addresses in storefronts\nand signs, in print and all varieties of visual media. Innovatively purchase\nseveral brand related names to showcase on billboards, banners, buses, bus\nstops, and blimps to effectively geo-target numerous markets simultaneously. Take\nadvantage of “WOW Deals” the inclusive marketing tool designed to boost business\nby creating and publishing Social buying deals on your site.<o:p></o:p></p>\n\n<p class="MsoNormal"><o:p>&nbsp;</o:p></p>\n\n<p class="MsoNormal" align="center">Sign up for W-Address\nand Design, Display and be Discovered today!</p>\n\n<p class="MsoNormal" align="center"><o:p>&nbsp;</o:p></p></div><div><b><br></b></div><div><br></div>', '', 'Video', '', '', 'en', 1);

-- --------------------------------------------------------

--
-- Table structure for table `deals`
--

CREATE TABLE IF NOT EXISTS `deals` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `waddress_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `waddress_id` (`waddress_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `deals`
--

INSERT INTO `deals` (`id`, `user_id`, `waddress_id`, `description`, `created`, `modified`) VALUES
(2, 100, 1, 'Type here you deal description :)', '2013-12-19 00:00:00', '2013-12-19 15:21:30'),
(19, 174, 143, '', '2014-02-05 00:00:00', '2014-02-05 14:22:36'),
(23, 190, 161, '', '2014-02-06 00:00:00', '2014-02-06 16:22:21'),
(25, 194, 168, 'Wows deal description here....', '2014-02-11 00:00:00', '2014-02-07 10:25:19'),
(27, 197, 172, 'Wows deal hereWows deal hereWows deal hereWows deal hereWows deal hereWows deal hereWows deal hereWows deal hereWows deal hereWows deal hereWows deal hereWow', '2014-02-11 00:00:00', '2014-02-11 11:17:19'),
(28, 202, 182, 'Hi this is tesing Wow Deal desc', '2014-02-13 06:09:52', '2014-02-12 12:19:20');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `waddress_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `subscription_id` varchar(200) NOT NULL,
  `paypal_email` varchar(500) NOT NULL,
  `transaction_token` varchar(255) NOT NULL,
  `price` float(10,4) NOT NULL,
  `payment_status` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `ipn_count` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`,`plan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `user_id`, `waddress_id`, `plan_id`, `subscription_id`, `paypal_email`, `transaction_token`, `price`, `payment_status`, `created`, `modified`, `ipn_count`) VALUES
(15, 133, 130, 3, '', 'yolopersonal@gmail.com', '81X18301KD378512K', 39.0000, 'Completed', '2014-02-04 00:00:00', '2014-02-04 16:51:45', 1),
(36, 174, 143, 3, '', 'yolopersonal@gmail.com', '0TA70455UB4472240', 39.0000, 'Completed', '2014-02-05 00:00:00', '2014-02-05 14:21:45', 1),
(43, 185, 149, 3, '', 'yolopersonal@gmail.com', '7RA59414UF999520W', 39.0000, 'Completed', '2014-02-11 00:00:00', '2014-02-06 16:00:43', 8),
(44, 190, 161, 3, '', 'yolopersonal@gmail.com', '8AP33376D64475135', 39.0000, 'Completed', '2014-02-06 00:00:00', '2014-02-06 16:21:52', 1),
(48, 194, 168, 3, '', 'yolopersonal@gmail.com', '2WM89185F3922525W', 39.0000, 'Completed', '2014-02-07 00:00:00', '2014-02-07 10:24:00', 1),
(52, 195, 170, 2, '', 'yolopersonal@gmail.com', '3FN84716MN8946014', 9.9500, 'Completed', '2014-02-10 00:00:00', '2014-02-10 12:07:49', 2),
(53, 196, 171, 2, '', 'yolopersonal@gmail.com', '14N426019R5620204', 19.9000, 'Completed', '2014-02-10 00:00:00', '2014-02-10 17:12:55', 3),
(54, 197, 172, 3, '', 'yolopersonal@gmail.com', '2R388099H54574308', 39.0000, 'Completed', '2014-02-11 00:00:00', '2014-02-11 11:17:07', 1),
(55, 198, 174, 2, '', 'yolopersonal@gmail.com', '0SA85428WP685121K', 9.9500, 'Completed', '2014-02-11 00:00:00', '2014-02-11 11:50:29', 1),
(56, 201, 181, 2, '', 'yolopersonal@gmail.com', '1GL35145TU478364P', 39.8000, 'Completed', '2014-02-12 00:00:00', '2014-02-12 11:53:58', 1),
(57, 202, 182, 3, '', 'yolopersonal@gmail.com', '833475379U022142Y', 39.0000, 'Completed', '2014-02-12 00:00:00', '2014-02-12 12:17:22', 1),
(58, 202, 183, 3, '', 'yolopersonal@gmail.com', '7PV25890UC232564A', 39.0000, 'Completed', '2014-02-12 00:00:00', '2014-02-12 12:21:51', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL auto_increment,
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`, `label`) VALUES
(12, 'fb_appid', '360088420751230', 'FB Appid'),
(13, 'fb_appsecret', '8a0dd726eb59f303bb27c8b250213dfa', 'FB AppSecretKey'),
(14, 'twitter_appid', 'KLX7fFC6aJod5w6b47EzZQ', 'Twitter appid'),
(15, 'twitter_appsecret', '0M9C99PwrNTH0IPsyFFqlqCSMJDw3uExCuxXMF4D10', 'Twitter AppsecretKey'),
(16, 'googleplus_appid', '3252234575-jn2h8jrog4lf55q5srdcju6f142vim75.apps.googleusercontent.com', 'Google+ appid'),
(17, 'googleplus_appsecret', 'vwY8Y3TGJS5XeUpJ3RAnF111', 'Google+ AppsecretKey'),
(18, 'flickr_appid', '615c4bd66cdeaabcad40fa8a1359fdbb', 'Flickr Appid'),
(19, 'flickr_appsecret', '62a0699dd9d5f136', 'Flickr Appsecret key'),
(24, 'foursquare_appid', 'HPUIPYLYGDCS4LJD44BGMHCK5PYXDGZPV2DSPSFC00F5IUA2', 'Foursquare Appid'),
(25, 'foursquare_appsecret', 'KBEMBSSPTHIQIQHPVFOHJNCWRYZ1Y5DHRPMCHOKNPIVWAFYI', 'Foursquare AppsecretKey'),
(26, 'linkedin_appid', '18ke98bon6u9', 'Linkedin Appid'),
(27, 'linkedin_appsecret', '40jnVVkTExhFZEcA', 'Linkedin AppsecretKey'),
(28, 'tumblr_appid', 'fFafMk49gon6Lv4b90UfKnvb9rsFi4esJjMizNRPpLX8RwXGFL', 'Tumblr Appid'),
(29, 'tumblr_appsecret', 'BUzmx1UziCIXz9zt1aqmtkwU45kqFJce4YOpMeCIa5f27ztq97', 'Tumblr AppsecretKey'),
(30, 'instagram_appid', 'e09aa390f249452b9b7d9727840a77e6', 'Instagram Appid'),
(31, 'instagram_appsecret', '70cd01c1455848428dac502326383bf4', 'Instagram AppsecretKey'),
(32, 'youtube_google_appid', '236011593770.apps.googleusercontent.com', 'Google Appid for Youtube '),
(33, 'youtube_google_appsecret', '6hrLMy26kPBUsSLKIMrWY8mK', 'Google Appsecret for Youtube '),
(34, 'youtube_developer_key', 'AI39si6R3RS_b5F4Fbw3ISm3zm0U6qO6IIvU_yez9Nn8SbAns-yqskxEn6i5T_08hbGmv5FpU4JgcyP6-ru8ldLn-hK_yjoLCQ', 'Youtube Developer key '),
(35, 'email_from', 'bhavesh.khanpara@indianic.com', 'Set Default email for send mail'),
(42, 'paypal_business_email', 'demo@indianic.com', 'paypal_business_email');

-- --------------------------------------------------------

--
-- Table structure for table `socials`
--

CREATE TABLE IF NOT EXISTS `socials` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `waddress_id` int(11) NOT NULL,
  `social_type` enum('Googleplus','Facebook','Twitter','Flickr','Youtube','Foursquare','Linkedin','Tumblr','Instagram','Googlemap','Yelp','Pinterest') NOT NULL,
  `profile_address` text NOT NULL,
  `latlong` varchar(500) NOT NULL,
  `status` enum('1','0') NOT NULL default '1',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `waddress_id` (`waddress_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `socials`
--

INSERT INTO `socials` (`id`, `user_id`, `waddress_id`, `social_type`, `profile_address`, `latlong`, `status`, `created`, `modified`) VALUES
(12, 117, 52, 'Googleplus', 'https://plus.google.com/101335637836131421430/posts', '', '1', '2013-12-24 14:45:40', '2013-12-24 14:44:19'),
(13, 117, 52, 'Pinterest', 'http://www.pinterest.com/jamesblunt', '', '1', '2013-12-24 14:45:53', '2013-12-24 14:44:32'),
(36, 178, 151, 'Facebook', 'https://www.facebook.com/mclem2k10', '', '1', '2014-02-06 06:18:51', '2014-02-06 06:17:57'),
(37, 191, 165, 'Facebook', 'https://www.facebook.com/amy.hart.908', '', '1', '2014-02-07 01:28:37', '2014-02-07 01:27:40'),
(39, 194, 168, 'Googlemap', 'ahmedabad', '23.0395677, 72.56600449999996', '1', '2014-02-07 10:25:59', '2014-02-07 10:25:01'),
(40, 198, 174, 'Googlemap', 'ahmedabad,gujarat,india', '23.0395677, 72.56600449999996', '1', '2014-02-11 11:48:21', '2014-02-11 11:47:11'),
(41, 200, 180, 'Facebook', 'https://www.facebook.com/profile.php?id=100004516291873', '', '1', '2014-02-12 11:41:33', '2014-02-12 11:40:21'),
(42, 201, 181, 'Googlemap', 'ahmedabad', '23.0395677, 72.56600449999996', '1', '2014-02-12 11:49:25', '2014-02-12 11:48:13'),
(43, 202, 182, 'Facebook', 'https://www.facebook.com/profile.php?id=100004516291873', '', '1', '2014-02-12 12:19:43', '2014-02-12 12:18:31'),
(44, 202, 182, 'Googlemap', 'ahmedabad', '23.0395677, 72.56600449999996', '1', '2014-02-12 12:19:59', '2014-02-12 12:18:46');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL auto_increment,
  `plan_id` varchar(20) NOT NULL,
  `role_id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `email_publish` tinyint(4) NOT NULL default '1',
  `password` varchar(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL COMMENT 'play role as a username',
  `profile_pic` varchar(255) NOT NULL default 'default/default-avatar.png',
  `w_address` varchar(255) NOT NULL,
  `intro_text` text NOT NULL,
  `area_code` int(11) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `phone_publish` int(11) NOT NULL default '1',
  `city` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `zipcode` int(11) NOT NULL,
  `address` text NOT NULL,
  `status` enum('1','0') NOT NULL COMMENT 'mail conformation status',
  `subcription_date` date NOT NULL,
  `expiry_date` date NOT NULL,
  `payment_type` enum('a','m') NOT NULL default 'm',
  `limit` int(11) NOT NULL,
  `created` date NOT NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `last_name` (`last_name`),
  KEY `plan_id` (`plan_id`),
  KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=203 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `plan_id`, `role_id`, `email`, `email_publish`, `password`, `first_name`, `last_name`, `profile_pic`, `w_address`, `intro_text`, `area_code`, `phone`, `phone_publish`, `city`, `state`, `zipcode`, `address`, `status`, `subcription_date`, `expiry_date`, `payment_type`, `limit`, `created`, `modified`) VALUES
(1, '0', 1, 'admin@indianic.com', 1, '1b363eeba055453e11102a3b3c83e904cd38ba42', 'admin', 'admin', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '1', '0000-00-00', '0000-00-00', 'a', 0, '0000-00-00', '2013-12-19 11:20:24'),
(117, 'personal', 2, 'bhavesh.khanpara@indianic.com', 1, 'a47857a46928d62424edaaffabcdf62adce754ab', '', 'Personal', '1387876601.png', '', ' I am a php developer with 5+ years commercial experience in PHP/MySQL – (LAMP). I have very good commercial experience in analysis, design, development, testing and implementation of web applications. Selfmade and good communicator which help me to work with clients at all levels of the business. I am very strict in meeting deadlines which is a critical aspect for any business. All my current work is developed using the latest version of PHP and MySQL and my code is upto the current standards, secure and safe from SQL injections or similar hacking attempts.', 0, '(079) 1234567', 1, '', '', 0, 'PHP Web Developer With 5+ Experiences', '1', '2013-12-24', '2014-12-24', 'm', 0, '2013-12-24', '2013-12-24 14:45:21'),
(122, 'social_commerce', 2, 'jtrainaldi@gmail.com', 1, 'b8e787ef3703c6fef7ecc7f395bfcf0af81fa1b6', '', 'jtrainaldi3', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '1', '2013-12-26', '2014-02-26', 'm', 0, '2013-12-26', '2013-12-26 21:58:53'),
(137, 'personal', 2, 'harleyshellos@msn.com', 1, '5db62b5821dc9f942d1fa8c75df1b8ad7ee9ac3d', '', 'AmyHart', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '0', '2014-01-08', '2015-01-08', 'm', 0, '2014-01-08', '2014-01-08 22:39:06'),
(139, 'personal', 2, 'fromthehartmodeling@msn.com', 1, '5db62b5821dc9f942d1fa8c75df1b8ad7ee9ac3d', '', 'AmyH', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '0', '2014-01-08', '2015-01-08', 'm', 0, '2014-01-08', '2014-01-08 23:04:04'),
(140, 'commercial', 2, 'mclem2k13@gmail.com', 1, '9fa1a989fe2bb26190885d249410222e4db44dd1', '', 'test009', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '1', '2014-01-09', '2015-01-09', 'm', 0, '2014-01-09', '2014-01-09 03:43:03'),
(142, 'personal', 2, 'fromthehartmarketing@yahoo.com', 1, '5db62b5821dc9f942d1fa8c75df1b8ad7ee9ac3d', '', 'Hart', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '0', '2014-01-09', '2015-01-09', 'm', 0, '2014-01-09', '2014-01-09 21:26:48'),
(143, 'personal', 2, 'fromthehartmarketing@yahoo.com', 1, '5db62b5821dc9f942d1fa8c75df1b8ad7ee9ac3d', '', 'Hart04', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '0', '2014-01-09', '2015-01-09', 'm', 0, '2014-01-09', '2014-01-09 22:15:07'),
(149, 'personal', 2, 'mclem2k13@gmail.com', 1, '9fa1a989fe2bb26190885d249410222e4db44dd1', '', 'test011', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '1', '2014-01-15', '2015-01-15', 'm', 0, '2014-01-15', '2014-01-15 03:29:02'),
(161, 'personal', 2, 'bhavesh.khanpara@indianic.com', 1, 'a47857a46928d62424edaaffabcdf62adce754ab', '', 'newtemplate', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '0', '2014-01-21', '2015-01-21', 'm', 0, '2014-01-21', '2014-01-21 11:17:52'),
(170, 'personal', 2, 'bhavesh.khanpara@indianic.com', 1, '5accaae2a8ca2571b64fe7c80be41031663db3de', '', 'personal_n', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '0', '2014-02-04', '2015-02-04', 'm', 0, '2014-02-04', '2014-02-04 16:31:09'),
(171, 'personal', 2, 'bhavesh.khanpara@indianic.com', 1, '5accaae2a8ca2571b64fe7c80be41031663db3de', '', 'bhavesh007', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '0', '2014-02-04', '2015-02-04', 'm', 0, '2014-02-04', '2014-02-04 16:32:24'),
(174, 'commercial', 2, 'bhavesh.khanpara@indianic.com', 1, 'a47857a46928d62424edaaffabcdf62adce754ab', '', 'new_c', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '1', '2014-02-05', '2015-02-05', 'm', 0, '2014-02-05', '2014-02-05 14:20:42'),
(178, 'personal', 2, 'mclem2k13@gmail.com', 1, '9fa1a989fe2bb26190885d249410222e4db44dd1', '', 'mobile003', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '1', '2014-02-06', '2015-02-06', 'm', 0, '2014-02-06', '2014-02-06 04:15:21'),
(179, 'personal', 2, 'bhavesh.khanpara@indianic.com', 1, '5accaae2a8ca2571b64fe7c80be41031663db3de', '', 'pw', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '0', '2014-02-06', '2015-02-06', 'm', 0, '2014-02-06', '2014-02-06 14:10:59'),
(182, 'personal', 2, 'bhavesh.khanpara@indianic.com', 1, '5accaae2a8ca2571b64fe7c80be41031663db3de', '', 'bhavesh0077', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '0', '2014-02-06', '2015-02-06', 'm', 0, '2014-02-06', '2014-02-06 15:39:28'),
(191, 'personal', 2, 'mclem2k13@gmail.com', 1, '9fa1a989fe2bb26190885d249410222e4db44dd1', '', 'Mobile004', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '1', '2014-02-07', '2015-02-07', 'm', 0, '2014-02-07', '2014-02-07 01:02:33'),
(193, 'commercial', 2, 'Hawlieharttreats@gmail.com', 1, 'b70c4aeab9e9b0c0b60340d14341c894e3edcad2', '', 'From the Hart', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '0', '2014-02-07', '2015-02-07', 'm', 0, '2014-02-07', '2014-02-07 08:23:15'),
(194, 'commercial', 2, 'bhavesh.khanpara@indianic.com', 1, '5accaae2a8ca2571b64fe7c80be41031663db3de', '', 'Cpayment', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '1', '2014-02-07', '2015-02-07', 'm', 0, '2014-02-07', '2014-02-07 10:23:11'),
(195, 'social_commerce', 2, 'bhavesh.khanpara@indianic.com', 1, '5accaae2a8ca2571b64fe7c80be41031663db3de', '', 'new_social', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '1', '2014-02-10', '2014-04-10', 'm', 0, '2014-02-10', '2014-02-10 10:56:16'),
(196, 'social_commerce', 2, 'bhavesh.khanpara@indianic.com', 1, '5accaae2a8ca2571b64fe7c80be41031663db3de', '', 'devid007', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '1', '2014-02-10', '2014-04-10', 'm', 0, '2014-02-10', '2014-02-10 17:02:39'),
(197, 'commercial', 2, 'bhavesh.khanpara@indianic.com', 1, '5accaae2a8ca2571b64fe7c80be41031663db3de', '', 'setsession', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '1', '2014-02-11', '2015-02-11', 'm', 0, '2014-02-11', '2014-02-11 11:08:35'),
(198, 'social_commerce', 2, 'bhavesh.khanpara@indianic.com', 1, '5accaae2a8ca2571b64fe7c80be41031663db3de', '', 'new_sc', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '1', '2014-02-11', '2014-04-11', 'm', 0, '2014-02-11', '2014-02-11 11:46:20'),
(200, 'personal', 2, 'bhavesh.khanpara@indianic.com', 1, 'a47857a46928d62424edaaffabcdf62adce754ab', '', 'nic_personal', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '1', '2014-02-12', '2015-02-12', 'm', 0, '2014-02-12', '2014-02-12 10:44:07'),
(201, 'social_commerce', 2, 'bhavesh.khanpara@indianic.com', 1, '5accaae2a8ca2571b64fe7c80be41031663db3de', '', 'Ryan002', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '1', '2014-02-12', '2014-04-12', 'm', 0, '2014-02-12', '2014-02-12 11:44:40'),
(202, 'commercial', 2, 'bhavesh.khanpara@indianic.com', 1, '5accaae2a8ca2571b64fe7c80be41031663db3de', '', 'Ryan003', 'default/default-avatar.png', '', '', 0, '', 1, '', '', 0, '', '1', '2014-02-12', '2015-02-12', 'm', 0, '2014-02-12', '2014-02-12 12:15:15');

-- --------------------------------------------------------

--
-- Table structure for table `waddresses`
--

CREATE TABLE IF NOT EXISTS `waddresses` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `w_address` varchar(255) NOT NULL,
  `social_count` int(11) NOT NULL,
  `status` enum('1','0') NOT NULL default '0',
  `subcription_date` date NOT NULL,
  `expiry_date` date NOT NULL,
  `subscription_period` int(11) default '2' COMMENT 'This filed is only used for manage Email templates for Social commerce plan...not used for any others plan or business logic   2-is two months freee subscription ',
  `created` date NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `waddress` (`w_address`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=186 ;

--
-- Dumping data for table `waddresses`
--

INSERT INTO `waddresses` (`id`, `user_id`, `w_address`, `social_count`, `status`, `subcription_date`, `expiry_date`, `subscription_period`, `created`, `modified`) VALUES
(170, 195, 'new_social', 0, '1', '2014-02-10', '2014-03-10', 4, '2014-02-10', '0000-00-00'),
(161, 190, 'commercial_007', 0, '1', '2014-02-06', '2015-02-06', 2, '2014-02-06', '0000-00-00'),
(172, 197, 'setsession', 0, '1', '2014-02-11', '2015-02-11', 2, '2014-02-11', '0000-00-00'),
(143, 174, 'new_c', 0, '1', '2014-02-05', '2015-02-05', 2, '2014-02-05', '0000-00-00'),
(165, 191, 'mobile004', 1, '1', '2014-02-07', '2015-02-07', 2, '2014-02-07', '0000-00-00'),
(52, 117, 'PersonalWaddress', 2, '1', '2014-02-21', '2015-02-27', 2, '2013-12-24', '2014-02-21'),
(151, 178, 'mobile003', 1, '1', '2014-02-06', '2015-02-06', 2, '2014-02-06', '0000-00-00'),
(168, 194, 'Cpayment', 1, '1', '2014-02-07', '2014-02-27', 2, '2014-02-07', '0000-00-00'),
(171, 196, 'devid007', 0, '1', '2013-09-10', '2014-02-15', 6, '2014-02-15', '0000-00-00'),
(174, 198, 'new_sc', 1, '1', '2014-02-11', '2014-03-11', 3, '2014-02-11', '0000-00-00'),
(181, 201, 'Ryan002', 1, '1', '2013-09-12', '2014-02-17', 6, '2014-02-12', '0000-00-00'),
(180, 200, 'Ryan001', 1, '1', '2014-02-12', '2015-02-12', 2, '2014-02-12', '0000-00-00'),
(182, 202, 'Ryan003_commercial', 2, '1', '2014-02-12', '2015-02-12', 2, '2014-02-12', '0000-00-00'),
(183, 202, 'Ryan003_secondname', 0, '1', '2014-02-12', '2015-02-12', 2, '2014-02-12', '0000-00-00');
